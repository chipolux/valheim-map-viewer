import QtQuick
import QtQuick.Window
import QtQuick.Controls as Controls
import Qt.labs.platform as Platform
import QtQuick.Layouts
import Valheim

Window {
    width: 1280
    height: 720
    visible: true
    title: mapData.name ? qsTr("%1 (%2)").arg(mapData.name).arg(
                              mapData.seed) : qsTr("Valheim Map Viewer")
    color: "#181425"

    Shortcut {
        sequence: "Ctrl+Shift+A"

        onActivated: mapView.toggleShowAll()
    }

    TextEdit {
        id: copyBuffer
        visible: false

        function copyText(value) {
            text = value
            selectAll()
            copy()
        }
    }

    Platform.FileDialog {
        id: fileDialog
        fileMode: Platform.FileDialog.OpenFile
        nameFilters: ["World FWL files (*.fwl)"]
        folder: mapData.mapDataFolder

        onAccepted: mapData.loadMap(file)
    }

    MapView {
        id: mapView
        map: mapData

        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: sidebarBackground.right
        anchors.right: parent.right

        MouseArea {
            property real startX: -1
            property real startY: -1
            property real lastX: -1
            property real lastY: -1

            onPressed: {
                startX = mouseX
                startY = mouseY
                lastX = mouseX
                lastY = mouseY
            }
            onPositionChanged: {
                var xDelta = mouseX - lastX
                var yDelta = mouseY - lastY
                if (pressed && (Math.abs(xDelta) > 1 || Math.abs(yDelta) > 1)) {
                    parent.scroll(Qt.point(xDelta, yDelta))
                    lastX = mouseX
                    lastY = mouseY
                }
            }
            onReleased: {
                var xDelta = Math.abs(mouseX - startX)
                var yDelta = Math.abs(mouseY - startY)
                if (xDelta < 5 && yDelta < 5) {
                    parent.select(mouseX, mouseY)
                }
                startX = -1
                startY = -1
                lastX = -1
                lastY = -1
            }
            onWheel: wheel => {
                         if (wheel.angleDelta.y > 0) {
                             parent.zoomIn(Qt.point(wheel.x, wheel.y))
                         } else {
                             parent.zoomOut(Qt.point(wheel.x, wheel.y))
                         }
                     }

            anchors.fill: parent
        }
    }

    Rectangle {
        color: "#AA000000"
        radius: 3
        width: childrenRect.width + 20
        height: childrenRect.height + 20

        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10

        Controls.Label {
            x: 10
            y: 10
            text: `Layer: ${mapView.currentLayerName}`
            font.pixelSize: 13
            color: "#BBB"

            MouseArea {
                onClicked: mapView.nextLayer()

                anchors.fill: parent
                anchors.margins: -10
            }
        }
    }

    Rectangle {
        id: selectedBiome
        visible: mapView.selectedBiome != "None"
        color: "#AA000000"
        width: biomeInfoColumn.width + 20
        height: biomeInfoColumn.height + 20
        radius: 3

        Column {
            id: biomeInfoColumn

            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.margins: 10

            Controls.Label {
                text: "Selected Biome: %1".arg(mapView.selectedBiome)
                font.pixelSize: 13
                color: "#BBB"
            }
        }

        anchors.left: mapView.left
        anchors.bottom: mapView.bottom
        anchors.margins: 10
    }

    Rectangle {
        id: sidebarBackground
        width: 300
        color: "#193c3e"

        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: parent.left
    }

    ColumnLayout {
        id: sidebarLayout
        spacing: 10

        Controls.Button {
            text: "Load World"
            font.pixelSize: 13
            z: entitiesList.z + 1

            onClicked: fileDialog.open()

            Layout.fillWidth: true
        }

        ListView {
            id: entitiesList
            model: mapView.entities
            spacing: 10
            clip: true
            delegate: Rectangle {
                color: "#AA000000"
                width: ListView.view.width
                height: entityInfoColumn.height + 20
                radius: 3
                border.color: {
                    switch (modelData.type) {
                    case MapEntity.PORTAL:
                        return "#c52052"
                    case MapEntity.SIGN:
                        return "#b86f50"
                    default:
                        return "transparent"
                    }
                }
                border.width: 3

                Column {
                    id: entityInfoColumn
                    Controls.Label {
                        text: "Entity %1 @ %2".arg(
                                  Number(modelData.id).toFixed(0)).arg(
                                  Number(modelData.offset).toFixed(0))
                        font.pixelSize: 13
                        color: "#BBB"

                        MouseArea {
                            onClicked: copyBuffer.copyText(
                                           mapData.getRawEntity(
                                               modelData.offset))

                            anchors.fill: parent
                        }
                    }

                    Controls.Label {
                        text: "Type: %1".arg(modelData.typeName)
                        font.pixelSize: 13
                        color: "#BBB"
                        leftPadding: 10

                        MouseArea {
                            onClicked: copyBuffer.copyText(
                                           Number(modelData.typeInt).toFixed(0))

                            anchors.fill: parent
                        }
                    }

                    Controls.Label {
                        text: "Position: (%1, %2, %3)".arg(
                                  Number(modelData.position.x).toFixed(1)).arg(
                                  Number(modelData.position.y).toFixed(1)).arg(
                                  Number(modelData.position.z).toFixed(1))
                        font.pixelSize: 13
                        color: "#BBB"
                        leftPadding: 10

                        MouseArea {
                            onClicked: mapView.zoomToPoint(
                                           modelData.flatPosition)

                            anchors.fill: parent
                        }
                    }

                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.margins: 10
                }
            }

            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        Controls.Label {
            text: "Scale: %1\nOffset: (%2, %3)".arg(
                      Number(mapView.scale).toFixed(2)).arg(
                      Number(mapView.offset.x).toFixed(0)).arg(
                      Number(mapView.offset.y).toFixed(0))
            font.pixelSize: 13
            color: "#BBB"
            z: entitiesList.z + 1

            Layout.fillWidth: true
        }

        Controls.Button {
            text: "Zoom To Fit"
            font.pixelSize: 13
            z: entitiesList.z + 1

            onClicked: mapView.fitMap()

            Layout.fillWidth: true
        }

        Controls.Button {
            text: "Zoom To Spawn"
            font.pixelSize: 13
            z: entitiesList.z + 1

            onClicked: mapView.zoomToSpawn()

            Layout.fillWidth: true
        }

        anchors.fill: sidebarBackground
        anchors.margins: 10
    }

    Column {
        id: structuresList
        spacing: 10

        Repeater {
            model: mapView.structures

            Rectangle {
                color: "#AA000000"
                width: 300
                height: structureInfoColumn.height + 20
                radius: 3

                Column {
                    id: structureInfoColumn
                    Controls.Label {
                        text: "%1 @ %2".arg(modelData.label).arg(
                                  Number(modelData.offset).toFixed(0))
                        font.pixelSize: 13
                        color: "#BBB"

                        MouseArea {
                            onClicked: copyBuffer.copyText(
                                           Number(modelData.offset).toFixed(0))

                            anchors.fill: parent
                        }
                    }

                    Controls.Label {
                        text: "Position: (%1, %2, %3)".arg(
                                  Number(modelData.position.x).toFixed(1)).arg(
                                  Number(modelData.position.y).toFixed(1)).arg(
                                  Number(modelData.position.z).toFixed(1))
                        font.pixelSize: 13
                        color: "#BBB"
                        leftPadding: 10

                        MouseArea {
                            onClicked: mapView.zoomToPoint(
                                           modelData.flatPosition)

                            anchors.fill: parent
                        }
                    }

                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.margins: 10
                }
            }
        }

        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.margins: 10
    }

    Controls.Dialog {
        id: errorDialog
        title: "Error"
        font.pixelSize: 13
        modal: true
        standardButtons: Controls.Dialog.Ok
        width: parent.width * 0.6

        Connections {
            target: mapData

            function onMapLoaded(error) {
                if (error) {
                    errorLabel.text = error
                    errorDialog.open()
                }
            }
        }

        Controls.Label {
            id: errorLabel
            font.pixelSize: 13
            wrapMode: Text.Wrap
            width: parent.width
        }

        anchors.centerIn: parent
    }
}
