#ifndef MAPSTRUCTURE_H
#define MAPSTRUCTURE_H

#include <QObject>
#include <QVector3D>

class MapStructure : public QObject
{
    Q_OBJECT

    Q_PROPERTY(qint64 offset READ offset CONSTANT)
    Q_PROPERTY(QString label READ label CONSTANT)
    Q_PROPERTY(TYPE type READ type CONSTANT)
    Q_PROPERTY(const QVector3D position READ position CONSTANT)
    Q_PROPERTY(const QPointF flatPosition READ flatPosition CONSTANT)
    Q_PROPERTY(bool built READ built CONSTANT)

  public:
    enum TYPE {
        START_TEMPLE,
        DRAUGR_VILLAGE,
        TROLL_CAVE,
        SUNKEN_CRYPT,
        BURIAL_CHAMBER,
        FULING_VILLAGE,
        DRAKE_NEST,
        HALDOR,
        EIKTHYR,
        THE_ELDER,
        BONEMASS,
        MODER,
        YAGLUTH,
        OTHER,
    };
    Q_ENUM(TYPE)

    explicit MapStructure(QDataStream &stream, QObject *parent = nullptr);

    qint64 offset() const { return m_offset; }
    const QString &label() const { return qAsConst(m_label); }
    const QVector3D position() const { return {m_xCoord, m_yCoord, m_zCoord}; }
    bool built() const { return m_built; }
    const QPointF flatPosition() const { return {m_xCoord, m_zCoord}; }
    TYPE type() const { return m_type; }

  private:
    qint64 m_offset;
    QString m_label;
    float m_xCoord;
    float m_yCoord;
    float m_zCoord;
    bool m_built;
    TYPE m_type;
};

#endif // MAPSTRUCTURE_H
