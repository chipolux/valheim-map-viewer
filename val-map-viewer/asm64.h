#ifndef ASM64_H
#define ASM64_H

#include <QString>
#include <algorithm>
#include <cstdint>

namespace asm64
{
union Reg32 {
    Reg32(int32_t v = 0)
        : i(v){};
    Reg32(float v)
        : f(v){};
    int32_t i;
    float f;
};

struct XmmReg {
    XmmReg(int32_t _a = 0, int32_t _b = 0, int32_t _c = 0, int32_t _d = 0)
        : a(_a)
        , b(_b)
        , c(_c)
        , d(_d){};
    XmmReg(float _a, float _b, float _c, float _d)
        : a(_a)
        , b(_b)
        , c(_c)
        , d(_d){};
    Reg32 a, b, c, d;
};

// move even indexed single-precision floats (duplicating)
void movsldup(const XmmReg &source, XmmReg &dest)
{
    dest.a.f = source.a.f;
    dest.b.f = source.a.f;
    dest.c.f = source.c.f;
    dest.d.f = source.c.f;
}

// move odd indexed single-precision floats (duplicating)
void movshdup(const XmmReg &source, XmmReg &dest)
{
    dest.a.f = source.b.f;
    dest.b.f = source.b.f;
    dest.c.f = source.d.f;
    dest.d.f = source.d.f;
}

// move aligned packed integers
void movdqa(const XmmReg &source, XmmReg &dest)
{
    dest.a.i = source.a.i;
    dest.b.i = source.b.i;
    dest.c.i = source.c.i;
    dest.d.i = source.d.i;
}

// logical and for packed values
void pand(const XmmReg &source, XmmReg &dest)
{
    dest.a.i &= source.a.i;
    dest.b.i &= source.b.i;
    dest.c.i &= source.c.i;
    dest.d.i &= source.d.i;
}

// logical or for packed values
void por(const XmmReg &source, XmmReg &dest)
{
    dest.a.i |= source.a.i;
    dest.b.i |= source.b.i;
    dest.c.i |= source.c.i;
    dest.d.i |= source.d.i;
}

// move aligned packed single-precision floats
void movaps(const XmmReg &source, XmmReg &dest)
{
    dest.a.f = source.a.f;
    dest.b.f = source.b.f;
    dest.c.f = source.c.f;
    dest.d.f = source.d.f;
}

// move one single-precision float
void movss(const XmmReg &source, XmmReg &dest) { dest.a.f = source.a.f; }

// add one single-precision float
void addss(const XmmReg &source, XmmReg &dest) { dest.a.f += source.a.f; }

// add packed single-precision floats
void addps(const XmmReg &source, XmmReg &dest)
{
    dest.a.f += source.a.f;
    dest.b.f += source.b.f;
    dest.c.f += source.c.f;
    dest.d.f += source.d.f;
}

// subtract one single-precision float
void subss(const XmmReg &source, XmmReg &dest) { dest.a.f -= source.a.f; }

// subtract packed single-precision floats
void subps(const XmmReg &source, XmmReg &dest)
{
    dest.a.f -= source.a.f;
    dest.b.f -= source.b.f;
    dest.c.f -= source.c.f;
    dest.d.f -= source.d.f;
}

// multiply one single-precision float
void mulss(const XmmReg &source, XmmReg &dest) { dest.a.f *= source.a.f; }

// multiply packed single-precision floats
void mulps(const XmmReg &source, XmmReg &dest)
{
    dest.a.f *= source.a.f;
    dest.b.f *= source.b.f;
    dest.c.f *= source.c.f;
    dest.d.f *= source.d.f;
}

// maximum of packed unsigned integers
void pmaxud(const XmmReg &source, XmmReg &dest)
{
    dest.a.i = std::max(dest.a.i, source.a.i);
    dest.b.i = std::max(dest.b.i, source.b.i);
    dest.c.i = std::max(dest.c.i, source.c.i);
    dest.d.i = std::max(dest.d.i, source.d.i);
}

// minimum of one single-precision float
void minss(const XmmReg &source, XmmReg &dest) { dest.a.f = std::min(dest.a.f, source.a.f); }

// compare packed data for equality
void pcmpeqd(const XmmReg &source, XmmReg &dest)
{
    dest.a.i = source.a.i == dest.a.i ? 0xFFFFFFFF : 0x00000000;
    dest.b.i = source.b.i == dest.b.i ? 0xFFFFFFFF : 0x00000000;
    dest.c.i = source.c.i == dest.c.i ? 0xFFFFFFFF : 0x00000000;
    dest.d.i = source.d.i == dest.d.i ? 0xFFFFFFFF : 0x00000000;
}

// blend packed single precision floats (based on mask in xmm0)
void blendvps(const XmmReg &mask, const XmmReg &source, XmmReg &dest)
{
    dest.a.f = (mask.a.i >> 31) == 0 ? dest.a.f : source.a.f;
    dest.b.f = (mask.b.i >> 31) == 0 ? dest.b.f : source.b.f;
    dest.c.f = (mask.c.i >> 31) == 0 ? dest.c.f : source.c.f;
    dest.d.f = (mask.d.i >> 31) == 0 ? dest.d.f : source.d.f;
}

// logical xor for packed single precision floats
void xorps(const XmmReg &source, XmmReg &dest)
{
    dest.a.i ^= source.a.i;
    dest.b.i ^= source.b.i;
    dest.c.i ^= source.c.i;
    dest.d.i ^= source.d.i;
}

// logical or for packed single precision floats
void orps(const XmmReg &source, XmmReg &dest)
{
    dest.a.i |= source.a.i;
    dest.b.i |= source.b.i;
    dest.c.i |= source.c.i;
    dest.d.i |= source.d.i;
}

// logical and for packed single precision floats
void andps(const XmmReg &source, XmmReg &dest)
{
    dest.a.i &= source.a.i;
    dest.b.i &= source.b.i;
    dest.c.i &= source.c.i;
    dest.d.i &= source.d.i;
}

// logical and not for packed single precision floats
void andnps(const XmmReg &source, XmmReg &dest)
{
    dest.a.i = ~dest.a.i & source.a.i;
    dest.b.i = ~dest.b.i & source.b.i;
    dest.c.i = ~dest.c.i & source.c.i;
    dest.d.i = ~dest.d.i & source.d.i;
}

// compare less-than for one single precision floats
void cmpltss(const XmmReg &source, XmmReg &dest)
{
    dest.a.i = dest.a.f < source.a.f ? 0xFFFFFFFF : 0x00000000;
}
} // namespace asm64

#endif // ASM64_H
