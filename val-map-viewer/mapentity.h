#ifndef MAPENTITY_H
#define MAPENTITY_H

#include <QObject>
#include <QQuaternion>
#include <QVector3D>

#include "valheim.h"

class MapEntity : public QObject
{
    Q_OBJECT

    Q_PROPERTY(qint64 offset READ offset CONSTANT)
    Q_PROPERTY(qint64 userId READ userId CONSTANT)
    Q_PROPERTY(quint32 id READ id CONSTANT)
    Q_PROPERTY(quint32 ownerRevision READ ownerRevision CONSTANT)
    Q_PROPERTY(quint32 dataRevision READ dataRevision CONSTANT)
    Q_PROPERTY(quint8 persistent READ persistent CONSTANT)
    Q_PROPERTY(qint64 owner READ owner CONSTANT)
    Q_PROPERTY(qint64 timeCreated READ timeCreated CONSTANT)
    Q_PROPERTY(qint32 pgwVersion READ pgwVersion CONSTANT)
    Q_PROPERTY(quint8 objectType READ objectType CONSTANT)
    Q_PROPERTY(quint8 distant READ distant CONSTANT)
    Q_PROPERTY(TYPE type READ type CONSTANT)
    Q_PROPERTY(quint32 typeInt READ type CONSTANT)
    Q_PROPERTY(const QString &typeName READ typeName CONSTANT)
    Q_PROPERTY(const QVector3D position READ position CONSTANT)
    Q_PROPERTY(const QVector3D rotation READ rotation CONSTANT)
    Q_PROPERTY(const QPointF flatPosition READ flatPosition CONSTANT)
    Q_PROPERTY(qint32 chunkX READ chunkX CONSTANT)
    Q_PROPERTY(qint32 chunkY READ chunkY CONSTANT)

  public:
    enum TYPE : quint32 {
        // 951398868 (crypt root?)
        // 4036512582 (root on ceiling)
        // 1620622954 (mud pile?)
        // 2154482690 (crypt draugr or blob spawn)
        // 959730964 (another crypt spawn, possibly blob)
        // 2583058517 (maybe mushroom spawn point? always in mushroom huts, and swamp)

        // 2823374043 Eikthyr Vegvisir at start temple, entity id 9 in ZEROO, just a custom rock

        // 183471488 was underneath/inside a boulder and disappeared after a save
        // 425751481 moved around between saves, maybe a troll?
        // 449644520 far away
        // 1103509118 far away
        // 1287973336 far away
        // 233964764 dolmen top stone?
        // 1554778960 dropped something?
        // 1800048705 some kind dolmen thing
        // 121363955 some shipwreck thing
        // 2034747966 some ruin thing, maybe barrel?
        // 2087336966 another dolmen thing
        // 2183831091 more shipwreck stuff
        // 222646934 eikthyr altar thing
        // 2783987236 greydwarf camp thing
        // 757858766 something in burial crypts
        // 3259876561 dropped something
        // 1673474691 something in stone ruins
        // 1396764178 more dolmen stuff
        // 2251715398 more burial crypt stuff
        // 81405021 one outside every burial crypt, maybe the cave entrance
        // 3017761529 one right inside every burial crypt, maybe the exit portal
        // 1577361568 more stuff inside burial crypts
        // 1405222666 in black forest, by water maybe?
        // 544574264 part of ruin2, lots clustered around
        // 449644525 more stuff inside burial crypts
        // 903556745 2 outside every burial crypt, and at stone ruins in black forest
        // 1477467946 more stuff inside burial crypts
        // 2213956272 more stuff inside burial crypts
        // 2014622831 more stuff inside burial crypts
        // 2813180547 more stuff inside burial crypts
        // 3948002384 more stuff inside burial crypts
        // 2346386137 more stuff inside burial crypts
        // 2158216306 greydwarf nest thing?
        // 3111660467 shipwreck thing?
        // 3445945039 shipwreck thing?
        // 3535165384 shipwreck thing?
        // 3176481554 something at center of woodfarm
        MYSTERY = 0,  // set to an unknown id so we can draw them for identification
        //        TYPE(1334057821) 1334057821 : 1
        //        TYPE(1457518369) 1457518369 : 1
        //        TYPE(-2025597564) 2269369732 : 1
        //        TYPE(-500516295) 3794451001 : 1

        // Start Temple Pillars
        EIKTHYR_PILLAR = 2895186737,
        THE_ELDER_PILLAR = 2510679832,
        BONEMASS_PILLAR = 2773857843,
        MODER_PILLAR = 1216631312,
        YAGLUTH_PILLAR = 3658753223,

        // Crops
        BARLEY = 967280542,
        FLAX = 3112034099,

        // Spawn Points (Maybe...)
        SPAWN_BOAR_1 = 1571435137,
        SPAWN_BOAR_2 = 2624099582,
        SPAWN_DEER = 291594142,
        SPAWN_FISH_1 = 109649205, // something in oceans and shallows, closer to shore
        SPAWN_FISH_2 = 109649206, // tons right near shorelines
        SPAWN_FISH_3 = 109649207, // something in oceans, mostly way far out
        SPAWN_GREYDWARF_1 = 1126707611,
        SPAWN_GREYDWARF_2 = 2363450701,
        SPAWN_GREYDWARF_BRUTE = 2920748937,
        SPAWN_GREYDWARF_SHAMAN = 1469476749,
        SPAWN_GREYLING = 4244006629,
        SPAWN_NECK = 2213908639,
        SPAWN_SKELETON = 449644526,
        SPAWN_SURTLING_1 = 489441179,  // around meteorite
        SPAWN_SURTLING_2 = 3516428644, // around firehole

        // Dropped Items
        DROPPED_AMBER = 2724219893,
        DROPPED_BEECH_SEEDS = 1912302441,
        DROPPED_BOAR_TROPHY = 434992580,
        DROPPED_BRONZE_NAILS = 2565576379,
        DROPPED_CHAIN = 1534575709,
        DROPPED_COAL = 204392453,
        DROPPED_DANDELION = 1288940892,
        DROPPED_DEER_HIDE = 705284766,
        DROPPED_DEER_TROPHY = 1281967652,
        DROPPED_ENTRAILS = 2309334920,
        DROPPED_FEATHERS = 2502243330,
        DROPPED_FINE_WOOD = 212315135,
        DROPPED_FLINT = 3342573409,
        DROPPED_FLINTHEAD_ARROW = 3451520050,
        DROPPED_GREYDWARF_EYE = 3468345236,
        DROPPED_IRON_NAILS = 584343451,
        DROPPED_LEATHER_SCRAPS = 1490625731,
        DROPPED_MUSHROOM = 2333069256,
        DROPPED_NECK_TAIL = 2896070579,
        DROPPED_OBSIDIAN = 4079386761,
        DROPPED_RAW_MEAT = 3628785785,
        DROPPED_RESIN = 3564310519,
        DROPPED_STONE = 3759435351,
        DROPPED_SURTLING_CORE_1 = 3389534007, // dropped by player
        DROPPED_SURTLING_CORE_2 = 1370511288, // dropped around fireholes and meteorites
        DROPPED_WOOD = 4143129795,
        DROPPED_SKELETAL_REMAINS = 1190462933,
        DROPPED_GREYDWARF_TROPHY = 1438359047,
        DROPPED_SKELETON_TROPHY = 446596857,
        DROPPED_TROLL_LEATHER = 2496353649,

        // Transports
        LONGSHIP = 118230510,
        KARVE = 3369438963,
        RAFT = 49675681,

        // Other Entities
        BURIAL_CRYPT_CHEST = 3800928356,
        GLOWING_METAL = 3323338689,
        LEVIATHAN = 1332964068,
        BARREL = 2034747966,
        ABANDONED_BED = 2296708057,
        ADZE = 4149750819,
        ALIEN_STATUE = 726946320,
        AMBER_PEARL = 1500324956,
        ANCIENT_TREE = 2111235755,
        ANVILS = 3873416157,
        BED = 3021628291,
        BEECH_LOG = 837895808,
        BEECH_SAPLING_1 = 1268841887,
        BEECH_SAPLING_2 = 865557360,
        BEECH_TREE = 3801705028,
        BELLOWS = 4276700684,
        BENCH = 2172159742,
        BIRCH_LOG = 2883055505,
        BIRCH_SAPLING = 3867481267,
        BIRCH_TREE = 2301397326,
        BLACK_FOREST_BUSH = 2515611895,
        BLUEBERRY_BUSH = 1998010392,
        BODY_PILE = 3895211066,
        BONEFIRE = 3295775803,
        BOULDER = 1172889253,
        BRANCH = 2035819856,
        BURIED_CHEST = 1182558977,
        BUSH = 548904977,
        CAMPFIRE = 399713608,
        CAULDRON = 2642476941,
        CHAIR = 2577545340,
        CHARCOAL_KILN = 816862660,
        CHOPPED_BEECH_LOG = 3670560988,
        CHOPPED_FIR_LOG = 3539218898,
        CHOPPING_BLOCK = 1017582937,
        CLINGING_MOSS = 3645086139,
        COLLAPSED_SWAMP_TREE = 1008438155,
        COOKING_STATION = 1430888227,
        COPPER_DEPOSIT = 2742875125,
        CRYPT_CHEST = 4104329089,
        CRYPT_DECORATION_1 = 4036512582,
        CRYPT_DECORATION_2 = 951398868,
        CRYPT_DECORATION_3 = 1620622954,
        CRYPT_DECORATION_4 = 144829813,
        CRYPT_DECORATION_5 = 548114341,
        CRYPT_EXIT = 3848155824,
        CRYPT_GATE = 3955100387,
        CRYPT_LOOT = 849594011,
        DANDELION = 16756586,
        FERMENTER = 3363575376,
        FIR_LOG = 3147690478,
        FIR_TREE = 888684615,
        FLINT = 3077229197,
        FORGE = 2764594453,
        FORGE_COOLER = 1951101856,
        FORGE_TOOLRACK = 1547817329,
        GREEN_IRON_TORCH = 3770503286,
        GRINDING_WHEEL = 1144532802,
        HANGING_BRAZIER = 997718274,
        HEARTH = 656210618,
        HUGE_BOULDER = 2898733904,
        HUGE_SNOWY_BOULDER = 2896501586,
        IRON_GATE = 2275296700,
        ITEM_STAND_HORIZONTAL = 1822362821,
        ITEM_STAND_VERTICAL = 3059847805,
        LARGE_BOULDER = 3504308704,
        LARGE_FIR_TREE = 1185163063,
        LARGE_MOSSY_BOULDER = 1707947207,
        LARGE_SNOWY_BOULDER = 2897831187,
        LOG_BEAM_2M = 3408810461,
        LOG_BEAM_4M = 214591025,
        LOG_POLE_2M = 2870733965,
        LOG_POLE_4M = 1024658248,
        MEADOWS_CHEST = 600954715,
        MOSSY_LOG = 1051479931,
        MOSSY_STUMP = 595427151,
        MOUNTAIN_CHEST = 4133822925,
        MUDDY_SCRAP_PILE_1 = 4252449299,
        MUDDY_SCRAP_PILE_2 = 1756993846,
        MUSHROOM = 1520650186,
        OAK_TREE = 735284842,
        OBSIDIAN_DEPOSIT = 820355464,
        PERSONAL_CHEST = 698259470,
        PINE_TREE = 797319082,
        PLANTED_CARROT = 600686073,
        PLANTED_TURNIP = 412034074,
        PORTAL = 3633084356,
        RASPBERRY_BUSH = 1809630058,
        RAVEN_THRONE = 743791676,
        REINFORCED_CHEST = 2850983774,
        ROCK_1 = 1576173780,
        ROCK_2 = 1649426798, // partially destroyed?
        ROCK_PILE = 3200616988,
        ROUNDPOLE_FENCE = 2213850195,
        SCONCE = 1038085717,
        SHARP_STAKES = 1059480620,
        SHIP_SETTING_BOULDER = 3142257721,
        SIGN = 1084866395,
        SILVER_DEPOSIT = 1611466255,
        SKELETAL_REMAINS = 449644523,
        SMALL_FIR_TREE = 3927100942,
        SMALL_STUMP = 2869758808,
        SMELTER = 900941850,
        SMITHS_ANVIL = 385017915,
        STAKE_WALL = 13326225,
        STANDING_IRON_TORCH = 261914232,
        STONE = 2823374043,
        STONECUTTER = 4260536919,
        STONE_ARCH = 3751971072,
        STONE_FLOOR_2x2 = 2048610715,
        STONE_GOLEM = 710649036,
        STONE_PILLAR = 2547793620,
        STONE_STAIR = 389771597,
        STONE_WALL_1x1 = 2530540801,
        STONE_WALL_2x1 = 2671703302,
        STONE_WALL_4x2 = 2228012823,
        STOOL = 3657552954,
        STRUCTURE = 3980975189,
        STUMP = 747762302,
        SWAMP_BUSH = 4095427394,
        SWAMP_TREE = 1707951228,
        TABLE = 915094071,
        TAME_BEEHIVE = 28120085,
        TANNING_RACK = 1420867464,
        TERRAIN = 1703108136,
        // collection placed in chunk when terrain is modified in new versions
        TERRAIN_MODIFICATION_COLLECTION = 3927902183,
        TERRAIN_MODIFICATION_1 = 2294719101, // dig with pick, old style
        TERRAIN_MODIFICATION_2 = 463677683,  // level ground, old style
        TERRAIN_MODIFICATION_3 = 2609898926,
        TERRAIN_MODIFICATION_4 = 2375273309,
        TERRAIN_MODIFICATION_5 = 456394941,  // stone path, old style
        TERRAIN_MODIFICATION_6 = 3289354277, // cultivate, old style
        TERRAIN_MODIFICATION_7 = 2174648828, // grass, old style
        THATCH_ROOF_26 = 2668381834,
        THATCH_ROOF_45 = 3417326012,
        THATCH_ROOF_ICORNER_26 = 3218178601,
        THATCH_ROOF_ICORNER_45 = 1132532351,
        THATCH_ROOF_OCORNER_26 = 3218385519,
        THATCH_ROOF_OCORNER_45 = 1139360637,
        THATCH_ROOF_RIDGE_26 = 2141320638,
        THATCH_ROOF_RIDGE_45 = 3696845160,
        THISTLE = 1175276079,
        TIN_DEPOSIT = 2412474708,
        TOMBSTONE = 2736654627,
        TOOL_SHELF = 614298410,
        TURNIP_SEEDS = 1947547595,
        WILD_BEEHIVE = 185590564,
        WOOD_BEAM_1M = 4139840501,
        WOOD_BEAM_26 = 1332601932,
        WOOD_BEAM_2M = 3185719019,
        WOOD_BEAM_45 = 1735886465,
        WOOD_CHEST = 328745978,
        WOOD_DOOR = 692106840,
        WOOD_DRAGON = 775398114,
        WOOD_FLOOR_1x1 = 3549834703,
        WOOD_FLOOR_2x2 = 3249042154,
        WOOD_GATE = 1007930775,
        WOOD_IRON_BEAM = 2795849761,
        WOOD_IRON_POLE = 2271347828,
        WOOD_LADDER = 3940414362,
        WOOD_POLE_1M = 1142703842,
        WOOD_POLE_2M = 524618856,
        WOOD_ROOF_CROSS_26 = 437925541,
        WOOD_ROOF_CROSS_45 = 3095574375,
        WOOD_STACK = 2155118536,
        WOOD_STAIR = 945264965,
        WOOD_TORCH = 1744483228,
        WOOD_WALL = 3771911221,
        WOOD_WALL_26 = 1882996769,
        WOOD_WALL_45 = 3076716939,
        WOOD_WALL_HALF = 3294620940,
        WORKBENCH = 3336957262,
        YELLOW_MUSHROOM = 2952831755,
        SHIPWRECK_CHEST = 3535165384,
    };
    Q_ENUM(TYPE)

    enum TABLE_KEY : quint32 {
        SIGN_LABEL = 3423761293,     // table 6
        CONTENTS = 3356102854,       // table 6
        TOMBSTONE_NAME = 1227488406, // table 6
        PORTAL_NAME = 696029674,     // table 6
    };
    Q_ENUM(TABLE_KEY)

    inline const static QMap<TYPE, QString> TypeNames = {
        {BURIAL_CRYPT_CHEST, "Burial Crypt Chest"},
        {GLOWING_METAL, "Glowing Metal Deposit"},
        {SPAWN_SURTLING_1, "Spawn Surtling"},
        {SPAWN_SURTLING_2, "Spawn Surtling"},
        {LEVIATHAN, "Leviathan"},
        {BARREL, "Barrel"},
        {SPAWN_FISH_1, "Spawn Fish"},
        {SPAWN_FISH_2, "Spawn Fish"},
        {SPAWN_FISH_3, "Spawn Fish"},
        {SHIPWRECK_CHEST, "Shipwreck Chest"},
        {DROPPED_SKELETAL_REMAINS, "Dropped Skeletal Remains"},
        {DROPPED_GREYDWARF_TROPHY, "Dropped Greydwarf Trophy"},
        {DROPPED_SKELETON_TROPHY, "Dropped Skeleton Trophy"},
        {DROPPED_TROLL_LEATHER, "Dropped Troll Leather"},
        {EIKTHYR_PILLAR, "Eikthyr Sacrificial Stone"},
        {THE_ELDER_PILLAR, "The Elder Sacrificial Stone"},
        {BONEMASS_PILLAR, "Bonemass Sacrificial Stone"},
        {MODER_PILLAR, "Moder Sacrificial Stone"},
        {YAGLUTH_PILLAR, "Yagluth Sacrificial Stone"},
        {LONGSHIP, "Longship"},
        {KARVE, "Karve"},
        {RAFT, "Raft"},
        {ABANDONED_BED, "Abandoned Bed"},
        {ADZE, "Adze"},
        {ALIEN_STATUE, "Alien Statue"},
        {AMBER_PEARL, "Amber Pearl"},
        {ANCIENT_TREE, "Ancient Tree"},
        {ANVILS, "Anvils"},
        {BARLEY, "Barley"},
        {BED, "Bed"},
        {BEECH_LOG, "Beech Log"},
        {BEECH_SAPLING_1, "Beech Sapling"},
        {BEECH_SAPLING_2, "Beech Sapling"},
        {BEECH_TREE, "Beech Tree"},
        {BELLOWS, "Bellows"},
        {BENCH, "Bench"},
        {BIRCH_LOG, "Birch Log"},
        {BIRCH_SAPLING, "Birch Sapling"},
        {BIRCH_TREE, "Birch Tree"},
        {BLACK_FOREST_BUSH, "Bush (Black Forest)"},
        {BLUEBERRY_BUSH, "Blueberry Bush"},
        {BODY_PILE, "Body Pile"},
        {BONEFIRE, "Bonefire"},
        {BOULDER, "Boulder"},
        {BRANCH, "Branch"},
        {BURIED_CHEST, "Buried Chest"},
        {BUSH, "Bush (Meadows)"},
        {CAMPFIRE, "Campfire"},
        {CAULDRON, "Cauldron"},
        {CHAIR, "Chair"},
        {CHARCOAL_KILN, "Charcoal Kiln"},
        {CHOPPED_BEECH_LOG, "Beech Log"},
        {CHOPPED_FIR_LOG, "Fir Log"},
        {CHOPPING_BLOCK, "Chopping Block"},
        {CLINGING_MOSS, "Clinging Moss"},
        {COLLAPSED_SWAMP_TREE, "Collapsed Swamp Tree"},
        {COOKING_STATION, "Cooking Station"},
        {COPPER_DEPOSIT, "Copper Deposit"},
        {CRYPT_CHEST, "Crypt Chest"},
        {CRYPT_DECORATION_1, "Crypt Decoration"},
        {CRYPT_DECORATION_2, "Crypt Decoration"},
        {CRYPT_DECORATION_3, "Crypt Decoration"},
        {CRYPT_DECORATION_4, "Crypt Decoration"},
        {CRYPT_DECORATION_5, "Crypt Decoration"},
        {CRYPT_EXIT, "Crypt Exit"},
        {CRYPT_GATE, "Crypt Gate"},
        {CRYPT_LOOT, "Crypt Loot"},
        {DANDELION, "Dandelion"},
        {DROPPED_AMBER, "Dropped Amber"},
        {DROPPED_BEECH_SEEDS, "Dropped Beech Seeds"},
        {DROPPED_BOAR_TROPHY, "Dropped Boar Trophy"},
        {DROPPED_BRONZE_NAILS, "Dropped Bronze Nails"},
        {DROPPED_CHAIN, "Dropped Chain"},
        {DROPPED_COAL, "Dropped Coal"},
        {DROPPED_DANDELION, "Dropped Dandelion"},
        {DROPPED_DEER_HIDE, "Dropped Deer Hide"},
        {DROPPED_DEER_TROPHY, "Dropped Deer Trophy"},
        {DROPPED_ENTRAILS, "Dropped Entrails"},
        {DROPPED_FEATHERS, "Dropped Feathers"},
        {DROPPED_FINE_WOOD, "Dropped Fine Wood"},
        {DROPPED_FLINT, "Dropped Flint"},
        {DROPPED_FLINTHEAD_ARROW, "Dropped Flinthead Arrow"},
        {DROPPED_GREYDWARF_EYE, "Dropped Greydwarf Eye"},
        {DROPPED_IRON_NAILS, "Dropped Iron Nails"},
        {DROPPED_LEATHER_SCRAPS, "Dropped Leather Scraps"},
        {DROPPED_MUSHROOM, "Dropped Mushroom"},
        {DROPPED_NECK_TAIL, "Dropped Neck Tail"},
        {DROPPED_OBSIDIAN, "Dropped Obsidian"},
        {DROPPED_RAW_MEAT, "Dropped Raw Meat"},
        {DROPPED_RESIN, "Dropped Resin"},
        {DROPPED_STONE, "Dropped Stone"},
        {DROPPED_SURTLING_CORE_1, "Dropped Surtling Core"},
        {DROPPED_SURTLING_CORE_2, "Dropped Surtling Core"},
        {DROPPED_WOOD, "Dropped Wood"},
        {FERMENTER, "Fermenter"},
        {FIR_LOG, "Fir Log"},
        {FIR_TREE, "Fir Tree"},
        {FLAX, "Flax"},
        {FLINT, "Flint"},
        {FORGE, "Forge"},
        {FORGE_COOLER, "Forge Cooler"},
        {FORGE_TOOLRACK, "Forge Toolrack"},
        {GREEN_IRON_TORCH, "Green Iron Torch"},
        {GRINDING_WHEEL, "Grinding Wheel"},
        {HANGING_BRAZIER, "Hanging Brazier"},
        {HEARTH, "Hearth"},
        {HUGE_BOULDER, "Huge Boulder"},
        {HUGE_SNOWY_BOULDER, "Huge Snowy Boulder"},
        {IRON_GATE, "Iron Gate"},
        {ITEM_STAND_HORIZONTAL, "Horizontal Item Stand"},
        {ITEM_STAND_VERTICAL, "Vertical Item Stand"},
        {LARGE_BOULDER, "Large Boulder"},
        {LARGE_FIR_TREE, "Large Fir Tree"},
        {LARGE_MOSSY_BOULDER, "Large Mossy Boulder"},
        {LARGE_SNOWY_BOULDER, "Large Snowy Boulder"},
        {LOG_BEAM_2M, "Log Beam 2M"},
        {LOG_BEAM_4M, "Log Beam 4M"},
        {LOG_POLE_2M, "Log Pole 2M"},
        {LOG_POLE_4M, "Log Pole 4M"},
        {MEADOWS_CHEST, "Meadows Chest"},
        {MOSSY_LOG, "Mossy Log"},
        {MOSSY_STUMP, "Mossy Stump"},
        {MOUNTAIN_CHEST, "Mountain Chest"},
        {MUDDY_SCRAP_PILE_1, "Muddy Scrap Pile"},
        {MUDDY_SCRAP_PILE_2, "Muddy Scrap Pile"},
        {MUSHROOM, "Mushroom"},
        {OAK_TREE, "Oak Tree"},
        {OBSIDIAN_DEPOSIT, "Obsidian Deposit"},
        {PERSONAL_CHEST, "Personal Chest"},
        {PINE_TREE, "Pine Tree"},
        {PLANTED_CARROT, "Planted Carrot"},
        {PLANTED_TURNIP, "Planted Turnip"},
        {PORTAL, "Portal"},
        {RASPBERRY_BUSH, "Raspberry Bush"},
        {RAVEN_THRONE, "Raven Throne"},
        {REINFORCED_CHEST, "Reinforced Chest"},
        {ROCK_1, "Rock"},
        {ROCK_2, "Rock"},
        {ROCK_PILE, "Rock Pile"},
        {ROUNDPOLE_FENCE, "Roundpole Fence"},
        {SCONCE, "Sconce"},
        {SHARP_STAKES, "Sharp Stakes"},
        {SHIP_SETTING_BOULDER, "Boulder"},
        {SIGN, "Sign"},
        {SILVER_DEPOSIT, "Silver Deposit"},
        {SKELETAL_REMAINS, "Skeletal Remains"},
        {SMALL_FIR_TREE, "Fir Tree"},
        {SMALL_STUMP, "Small Stump"},
        {SMELTER, "Smelter"},
        {SMITHS_ANVIL, "Smith's Anvil"},
        {SPAWN_BOAR_1, "Spawn Boar"},
        {SPAWN_BOAR_2, "Spawn Boar"},
        {SPAWN_DEER, "Spawn Deer"},
        {SPAWN_GREYDWARF_1, "Spawn Greydwarf"},
        {SPAWN_GREYDWARF_2, "Spawn Greydwarf"},
        {SPAWN_GREYDWARF_BRUTE, "Spawn Greydwarf Brute"},
        {SPAWN_GREYDWARF_SHAMAN, "Spawn Greydwarf Shaman"},
        {SPAWN_GREYLING, "Spawn Greyling"},
        {SPAWN_NECK, "Spawn Neck"},
        {STAKE_WALL, "Stake Wall"},
        {STANDING_IRON_TORCH, "Standing Iron Torch"},
        {STONE, "Stone"},
        {STONECUTTER, "Stonecutter"},
        {STONE_ARCH, "Stone Arch"},
        {STONE_FLOOR_2x2, "Stone Floor 2x2"},
        {STONE_GOLEM, "Stone Golem"},
        {STONE_PILLAR, "Stone Pillar"},
        {STONE_STAIR, "Stone Stair"},
        {STONE_WALL_1x1, "Stone Wall 1x1"},
        {STONE_WALL_2x1, "Stone Wall 2x1"},
        {STONE_WALL_4x2, "Stone Wall 4x2"},
        {STOOL, "Stool"},
        {STRUCTURE, "Structure"},
        {STUMP, "Stump"},
        {SWAMP_BUSH, "Bush (Swamp)"},
        {SWAMP_TREE, "Swamp Tree"},
        {TABLE, "Table"},
        {TAME_BEEHIVE, "Beehive"},
        {TANNING_RACK, "Tanning Rack"},
        {TERRAIN, "Terrain"},
        {TERRAIN_MODIFICATION_COLLECTION, "Terrain Modification (Collection)"},
        {TERRAIN_MODIFICATION_1, "Terrain Modification (Dig)"},
        {TERRAIN_MODIFICATION_2, "Terrain Modification (Level)"},
        {TERRAIN_MODIFICATION_3, "Terrain Modification"},
        {TERRAIN_MODIFICATION_4, "Terrain Modification"},
        {TERRAIN_MODIFICATION_5, "Terrain Modification (Stone Path)"},
        {TERRAIN_MODIFICATION_6, "Terrain Modification (Cultivate)"},
        {TERRAIN_MODIFICATION_7, "Terrain Modification (Grass)"},
        {THATCH_ROOF_26, "Thatch Roof 26°"},
        {THATCH_ROOF_45, "Thatch Roof 45°"},
        {THATCH_ROOF_ICORNER_26, "Thatch Roof I-Corner 26°"},
        {THATCH_ROOF_ICORNER_45, "Thatch Roof I-Corner 45°"},
        {THATCH_ROOF_OCORNER_26, "Thatch Roof O-Corner 26°"},
        {THATCH_ROOF_OCORNER_45, "Thatch Roof O-Corner 45°"},
        {THATCH_ROOF_RIDGE_26, "Thatch Roof Ridge 26°"},
        {THATCH_ROOF_RIDGE_45, "Thatch Roof Ridge 45°"},
        {THISTLE, "Thistle"},
        {TIN_DEPOSIT, "Tin Deposit"},
        {TOMBSTONE, "Tombstone"},
        {TOOL_SHELF, "Tool Shelf"},
        {TURNIP_SEEDS, "Turnip Seeds"},
        {WILD_BEEHIVE, "Wild Beehive"},
        {WOOD_BEAM_1M, "Wood Beam 1M"},
        {WOOD_BEAM_26, "Wood Beam 26°"},
        {WOOD_BEAM_2M, "Wood Beam 2M"},
        {WOOD_BEAM_45, "Wood Beam 45°"},
        {WOOD_CHEST, "Wood Chest"},
        {WOOD_DOOR, "Wood Door"},
        {WOOD_DRAGON, "Wood Dragon"},
        {WOOD_FLOOR_1x1, "Wood Floor 1x1"},
        {WOOD_FLOOR_2x2, "Wood Floor 2x2"},
        {WOOD_GATE, "Wood Gate"},
        {WOOD_IRON_BEAM, "Wood Iron Beam"},
        {WOOD_IRON_POLE, "Wood Iron Pole"},
        {WOOD_LADDER, "Wood Ladder"},
        {WOOD_POLE_1M, "Wood Pole 1M"},
        {WOOD_POLE_2M, "Wood Pole 2M"},
        {WOOD_ROOF_CROSS_26, "Wood Roof Cross 26°"},
        {WOOD_ROOF_CROSS_45, "Wood Roof Cross 45°"},
        {WOOD_STACK, "Wood Stack"},
        {WOOD_STAIR, "Wood Stair"},
        {WOOD_TORCH, "Wood Torch"},
        {WOOD_WALL, "Wood Wall"},
        {WOOD_WALL_26, "Wood Wall 26°"},
        {WOOD_WALL_45, "Wood Wall 45°"},
        {WOOD_WALL_HALF, "Wood Wall Half"},
        {WORKBENCH, "Workbench"},
        {YELLOW_MUSHROOM, "Yellow Mushroom"},
    };

    explicit MapEntity(const QByteArray &data, const qint64 &offset, const qint64 &userId,
                       const quint32 &id, const quint32 &mapVersion, QObject *parent = nullptr);

    qint64 offset() const { return m_offset; }
    qint64 userId() const { return m_userId; }
    quint32 id() const { return m_id; }
    quint32 ownerRevision() const { return m_ownerRevision; }
    quint32 dataRevision() const { return m_dataRevision; }
    quint8 persistent() const { return m_persistent; }
    qint64 owner() const { return m_owner; }
    qint64 timeCreated() const { return m_timeCreated; }
    qint32 pgwVersion() const { return m_pgwVersion; }
    quint8 objectType() const { return m_objectType; }
    quint8 distant() const { return m_distant; }
    TYPE type() const { return m_type; }
    const QString typeName() const
    {
        QString result = "";
        switch (m_type) {
        case TOMBSTONE:
            result = QString("Tombstone: %1").arg(m_strings[TOMBSTONE_NAME]);
            break;
        case SIGN:
            result = QString("Sign: %1").arg(m_strings[SIGN_LABEL]);
            break;
        case PORTAL:
            result = QString("Portal: %1").arg(m_strings[PORTAL_NAME]);
            break;
        default:
            result = TypeNames.value(m_type, QString::number(m_type));
            break;
        }
        return result;
    }
    const QVector3D position() const { return {m_xCoord, m_yCoord, m_zCoord}; }
    const QVector3D rotation() const { return {m_xRotEul, m_yRotEul, m_zRotEul}; }
    const QPointF flatPosition() const { return {m_xCoord, m_zCoord}; }
    const QPair<qint32, qint32> &chunk() const { return m_chunk; }
    qint32 chunkX() const { return m_chunk.first; }
    qint32 chunkY() const { return m_chunk.second; }

    // only used for terrain blocks at the moment
    Valheim::Biome nwBiome() const { return m_nwBiome; }
    void setNWBiome(Valheim::Biome biome) { m_nwBiome = biome; }
    Valheim::Biome neBiome() const { return m_neBiome; }
    void setNEBiome(Valheim::Biome biome) { m_neBiome = biome; }
    Valheim::Biome swBiome() const { return m_swBiome; }
    void setSWBiome(Valheim::Biome biome) { m_swBiome = biome; }
    Valheim::Biome seBiome() const { return m_seBiome; }
    void setSEBiome(Valheim::Biome biome) { m_seBiome = biome; }
    inline bool allMatch() const { return topMatch() && bottomMatch() && leftMatch(); }
    inline bool topMatch() const { return m_nwBiome == m_neBiome; }
    inline bool bottomMatch() const { return m_swBiome == m_seBiome; }
    inline bool leftMatch() const { return m_nwBiome == m_swBiome; }
    inline bool rightMatch() const { return m_neBiome == m_seBiome; }

  private:
    // long == qint64
    // uint == quint32
    // int == qint32
    // sbyte == char/quint8/qint8
    // char == char/quint8/qint8
    // refer to ZDOMan.SaveAsync() and ZDO.Save() in assembly_valheim.dll
    qint64 m_offset; // byte offset in world file
    qint64 m_userId;
    quint32 m_id;
    qint32 m_bodyLength;
    quint32 m_ownerRevision;
    quint32 m_dataRevision;
    quint8 m_persistent; // bool
    qint64 m_owner;
    qint64 m_timeCreated;
    qint32 m_pgwVersion;
    quint8 m_objectType;           // enum Default, Prioritized, Solid, Terrain
    quint8 m_distant;              // bool
    TYPE m_type;                   // m_prefab, should be qint32, but will change later
    QPair<qint32, qint32> m_chunk; // m_sector
    float m_xCoord;
    float m_yCoord;
    float m_zCoord;
    float m_xRot;
    float m_yRot;
    float m_zRot;
    float m_wRot;
    float m_xRotEul;
    float m_yRotEul;
    float m_zRotEul;
    QMap<qint32, float> m_floats;
    QMap<qint32, QVector3D> m_vecs;
    QMap<qint32, QQuaternion> m_quats;
    QMap<qint32, qint32> m_ints;
    QMap<qint32, qint64> m_longs;
    QMap<qint32, QString> m_strings;
    QMap<qint32, QByteArray> m_byteArrays;

    Valheim::Biome m_nwBiome;
    Valheim::Biome m_neBiome;
    Valheim::Biome m_swBiome;
    Valheim::Biome m_seBiome;
};

#endif // MAPENTITY_H
