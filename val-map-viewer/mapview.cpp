#include "mapview.h"

#include <QPainter>
#include <QPainterPath>
#include <QTimer>

#include "valheim.h"

MapView::MapView(QQuickItem *parent)
    : QQuickPaintedItem(parent)
    , m_map(nullptr)
    , m_visibleRect()
    , m_selectionRect()
    , m_scale(1)
    , m_xOffset(0)
    , m_zOffset(0)
    , m_currentLayer(LAYER_WORLD)
    , m_showAll(false)
    , m_portalIcon(":/resources/portal.png")
    , m_startTempleIcon(":/resources/start-temple.png")
    , m_copperDepositIcon(":/resources/copper-deposit.png")
    , m_tinDepositIcon(":/resources/tin-deposit.png")
    , m_mudPileIcon(":/resources/mud-pile.png")
    , m_silverDepositIcon(":/resources/silver-deposit.png")
    , m_draugrVillageIcon(":/resources/draugr-village.png")
    , m_trollCaveIcon(":/resources/troll-cave.png")
    , m_sunkenCryptIcon(":/resources/sunken-crypt.png")
    , m_burialChamberIcon(":/resources/burial-chamber.png")
    , m_fulingVillageIcon(":/resources/fuling-village.png")
    , m_drakeNestIcon(":/resources/drake-nest.png")
    , m_leviathanIcon(":/resources/leviathan.png")
    , m_haldorIcon(":/resources/haldor.png")
    , m_eikthyrIcon(":/resources/eikthyr.png")
    , m_theElderIcon(":/resources/the-elder.png")
    , m_bonemassIcon(":/resources/bonemass.png")
    , m_moderIcon(":/resources/moder.png")
    , m_yagluthIcon(":/resources/yagluth.png")
    , m_cryptTypes(
          {MapStructure::SUNKEN_CRYPT, MapStructure::BURIAL_CHAMBER, MapStructure::TROLL_CAVE})
    , m_overviewTypes({MapStructure::START_TEMPLE, MapStructure::EIKTHYR, MapStructure::THE_ELDER,
                       MapStructure::BONEMASS, MapStructure::MODER, MapStructure::YAGLUTH,
                       MapStructure::HALDOR})
{
    const QPointF iconPos(-12.0f, -12.0f);
    const QPicture emptyPicture;
    QPainter painter;

    // entity pictures
    {
        painter.begin(&m_defaultEntityPicture);
        painter.fillRect(QRectF(-0.5f, -0.5f, 1.0f, 1.0f), QColor(0x12, 0x4e, 0x89));
        painter.end();

        painter.begin(&m_entityPictures[MapEntity::LONGSHIP]);
        painter.setPen(Qt::NoPen);
        painter.setBrush(QColor(0xbe, 0x4a, 0x2f));
        painter.drawEllipse(QRectF(-2.25f, -8.0f, 4.5f, 16.0f));
        painter.end();

        painter.begin(&m_entityPictures[MapEntity::KARVE]);
        painter.setPen(Qt::NoPen);
        painter.setBrush(QColor(0xbe, 0x4a, 0x2f));
        painter.drawEllipse(QRectF(-1.6f, -4.5f, 3.2f, 9.0f));
        painter.end();

        painter.begin(&m_entityPictures[MapEntity::RAFT]);
        painter.fillRect(QRectF(-2.0f, -3.0f, 4.0f, 6.0f), QColor(0xbe, 0x4a, 0x2f));
        painter.end();

        painter.begin(&m_entityPictures[MapEntity::WOOD_WALL]);
        painter.fillRect(QRectF(-1.0f, -0.125f, 2.0f, 0.35f), QColor(0xbe, 0x4a, 0x2f));
        painter.end();
        m_entityPictures[MapEntity::WOOD_WALL_HALF] = m_entityPictures[MapEntity::WOOD_WALL];
        m_entityPictures[MapEntity::WOOD_WALL_26] = m_entityPictures[MapEntity::WOOD_WALL];
        m_entityPictures[MapEntity::WOOD_WALL_45] = m_entityPictures[MapEntity::WOOD_WALL];

        painter.begin(&m_entityPictures[MapEntity::COOKING_STATION]);
        painter.fillRect(QRectF(-1.0f, -0.4f, 2.0f, 0.8f), QColor(0xbe, 0x4a, 0x2f).darker(150));
        painter.end();

        painter.begin(&m_entityPictures[MapEntity::WOOD_DOOR]);
        painter.fillRect(QRectF(-1.0f, -0.125f, 2.0f, 0.35f), QColor(0xbe, 0x4a, 0x2f).darker(150));
        painter.end();
        m_entityPictures[MapEntity::WOOD_GATE] = m_entityPictures[MapEntity::WOOD_DOOR];

        painter.begin(&m_entityPictures[MapEntity::WOOD_TORCH]);
        painter.fillRect(QRectF(-0.1f, -0.1f, 0.2f, 0.2f), QColor(0xfe, 0xe7, 0x61));
        painter.end();
        m_entityPictures[MapEntity::GREEN_IRON_TORCH] = m_entityPictures[MapEntity::WOOD_TORCH];
        m_entityPictures[MapEntity::STANDING_IRON_TORCH] = m_entityPictures[MapEntity::WOOD_TORCH];

        painter.begin(&m_entityPictures[MapEntity::WOOD_FLOOR_1x1]);
        painter.fillRect(QRectF(-0.5f, -0.5f, 1.0f, 1.0f), QColor(0xbe, 0x4a, 0x2f, 0x22));
        painter.end();

        painter.begin(&m_entityPictures[MapEntity::WOOD_FLOOR_2x2]);
        painter.fillRect(QRectF(-1.0f, -1.0f, 2.0f, 2.0f), QColor(0xbe, 0x4a, 0x2f, 0x22));
        painter.end();
        m_entityPictures[MapEntity::WOOD_STAIR] = m_entityPictures[MapEntity::WOOD_FLOOR_2x2];

        painter.begin(&m_entityPictures[MapEntity::WOOD_CHEST]);
        painter.fillRect(QRectF(-0.8f, -0.4f, 1.6f, 0.8f), QColor(0x73, 0x3e, 0x39));
        painter.end();
        m_entityPictures[MapEntity::BURIED_CHEST] = m_entityPictures[MapEntity::WOOD_CHEST];
        m_entityPictures[MapEntity::CRYPT_CHEST] = m_entityPictures[MapEntity::WOOD_CHEST];
        m_entityPictures[MapEntity::MEADOWS_CHEST] = m_entityPictures[MapEntity::WOOD_CHEST];
        m_entityPictures[MapEntity::MOUNTAIN_CHEST] = m_entityPictures[MapEntity::WOOD_CHEST];
        m_entityPictures[MapEntity::SHIPWRECK_CHEST] = m_entityPictures[MapEntity::WOOD_CHEST];
        m_entityPictures[MapEntity::BURIAL_CRYPT_CHEST] = m_entityPictures[MapEntity::WOOD_CHEST];

        painter.begin(&m_entityPictures[MapEntity::STONE_FLOOR_2x2]);
        painter.fillRect(QRectF(-1.0f, -1.0f, 2.0f, 2.0f), QColor(0x8b, 0x9b, 0xb4, 0x22));
        painter.end();
        m_entityPictures[MapEntity::STONE_STAIR] = m_entityPictures[MapEntity::STONE_FLOOR_2x2];

        painter.begin(&m_entityPictures[MapEntity::STONE_WALL_1x1]);
        painter.fillRect(QRectF(-0.5f, -0.5f, 1.0f, 1.0f), QColor(0x8b, 0x9b, 0xb4));
        painter.end();
        m_entityPictures[MapEntity::STONE_PILLAR] = m_entityPictures[MapEntity::STONE_WALL_1x1];

        painter.begin(&m_entityPictures[MapEntity::STONE_WALL_2x1]);
        painter.fillRect(QRectF(-1.0f, -0.5f, 2.0f, 1.0f), QColor(0x8b, 0x9b, 0xb4));
        painter.end();

        painter.begin(&m_entityPictures[MapEntity::STONE_WALL_4x2]);
        painter.fillRect(QRectF(-2.0f, -0.5f, 4.0f, 1.0f), QColor(0x8b, 0x9b, 0xb4));
        painter.end();

        painter.begin(&m_entityPictures[MapEntity::PORTAL]);
        painter.drawImage(iconPos, m_portalIcon);
        painter.end();

        painter.begin(&m_entityPictures[MapEntity::COPPER_DEPOSIT]);
        painter.drawImage(iconPos, m_copperDepositIcon);
        painter.end();

        painter.begin(&m_entityPictures[MapEntity::SILVER_DEPOSIT]);
        painter.drawImage(iconPos, m_silverDepositIcon);
        painter.end();

        painter.begin(&m_entityPictures[MapEntity::MUDDY_SCRAP_PILE_1]);
        painter.drawImage(iconPos, m_mudPileIcon);
        painter.end();

        painter.begin(&m_entityPictures[MapEntity::MUDDY_SCRAP_PILE_2]);
        painter.drawImage(iconPos, m_mudPileIcon);
        painter.end();

        painter.begin(&m_entityPictures[MapEntity::TIN_DEPOSIT]);
        painter.drawImage(iconPos, m_tinDepositIcon);
        painter.end();

        painter.begin(&m_entityPictures[MapEntity::LEVIATHAN]);
        painter.drawImage(iconPos, m_leviathanIcon);
        painter.end();

        painter.begin(&m_entityPictures[MapEntity::RASPBERRY_BUSH]);
        painter.fillRect(QRectF(-0.5f, -0.5f, 1.0f, 1.0f), QColor(0xff, 0x00, 0x44));
        painter.end();

        painter.begin(&m_entityPictures[MapEntity::BLUEBERRY_BUSH]);
        painter.fillRect(QRectF(-0.5f, -0.5f, 1.0f, 1.0f), QColor(0x00, 0x99, 0xdb));
        painter.end();

        painter.begin(&m_entityPictures[MapEntity::BIRCH_TREE]);
        painter.fillRect(QRectF(-0.5f, -0.5f, 1.0f, 1.0f), QColor(0xc0, 0xcb, 0xdc));
        painter.end();

        painter.begin(&m_entityPictures[MapEntity::OAK_TREE]);
        painter.fillRect(QRectF(-0.5f, -0.5f, 1.0f, 1.0f), QColor(0xb8, 0x6f, 0x50));
        painter.end();

        painter.begin(&m_entityPictures[MapEntity::MYSTERY]);
        painter.fillRect(QRectF(-10.0f, -10.0f, 20.0f, 20.0f), QColor(0xb5, 0x50, 0x88, 0x55));
        painter.fillRect(QRectF(-0.5f, -0.5f, 1.0f, 1.0f), QColor(0xff, 0xff, 0xff));
        painter.end();

        // don't need to draw anything for these...
        m_entityPictures[MapEntity::DANDELION] = emptyPicture;
        m_entityPictures[MapEntity::BRANCH] = emptyPicture;
        m_entityPictures[MapEntity::STONE] = emptyPicture;
        m_entityPictures[MapEntity::BUSH] = emptyPicture;
        m_entityPictures[MapEntity::FLINT] = emptyPicture;
        m_entityPictures[MapEntity::BLACK_FOREST_BUSH] = emptyPicture;
        m_entityPictures[MapEntity::SWAMP_BUSH] = emptyPicture;
        m_entityPictures[MapEntity::BEECH_SAPLING_1] = emptyPicture;
        m_entityPictures[MapEntity::BEECH_SAPLING_2] = emptyPicture;
        m_entityPictures[MapEntity::BIRCH_SAPLING] = emptyPicture;
        m_entityPictures[MapEntity::ROCK_1] = emptyPicture;
        m_entityPictures[MapEntity::ROCK_2] = emptyPicture;
        m_entityPictures[MapEntity::CLINGING_MOSS] = emptyPicture;
        m_entityPictures[MapEntity::THATCH_ROOF_26] = emptyPicture;
        m_entityPictures[MapEntity::THATCH_ROOF_45] = emptyPicture;
        m_entityPictures[MapEntity::THATCH_ROOF_ICORNER_26] = emptyPicture;
        m_entityPictures[MapEntity::THATCH_ROOF_ICORNER_45] = emptyPicture;
        m_entityPictures[MapEntity::THATCH_ROOF_OCORNER_26] = emptyPicture;
        m_entityPictures[MapEntity::THATCH_ROOF_OCORNER_45] = emptyPicture;
        m_entityPictures[MapEntity::THATCH_ROOF_RIDGE_26] = emptyPicture;
        m_entityPictures[MapEntity::THATCH_ROOF_RIDGE_45] = emptyPicture;
        m_entityPictures[MapEntity::LOG_BEAM_2M] = emptyPicture;
        m_entityPictures[MapEntity::LOG_BEAM_4M] = emptyPicture;
        m_entityPictures[MapEntity::LOG_POLE_2M] = emptyPicture;
        m_entityPictures[MapEntity::LOG_POLE_4M] = emptyPicture;
        m_entityPictures[MapEntity::WOOD_POLE_1M] = emptyPicture;
        m_entityPictures[MapEntity::WOOD_POLE_2M] = emptyPicture;
        m_entityPictures[MapEntity::WOOD_BEAM_1M] = emptyPicture;
        m_entityPictures[MapEntity::WOOD_BEAM_2M] = emptyPicture;
        m_entityPictures[MapEntity::WOOD_BEAM_26] = emptyPicture;
        m_entityPictures[MapEntity::WOOD_BEAM_45] = emptyPicture;
        m_entityPictures[MapEntity::WOOD_DRAGON] = emptyPicture;
        m_entityPictures[MapEntity::WOOD_ROOF_CROSS_26] = emptyPicture;
        m_entityPictures[MapEntity::WOOD_ROOF_CROSS_45] = emptyPicture;
        m_entityPictures[MapEntity::TERRAIN_MODIFICATION_COLLECTION] = emptyPicture;
        m_entityPictures[MapEntity::TERRAIN_MODIFICATION_1] = emptyPicture;
        m_entityPictures[MapEntity::TERRAIN_MODIFICATION_2] = emptyPicture;
        m_entityPictures[MapEntity::TERRAIN_MODIFICATION_3] = emptyPicture;
        m_entityPictures[MapEntity::TERRAIN_MODIFICATION_4] = emptyPicture;
        m_entityPictures[MapEntity::TERRAIN_MODIFICATION_5] = emptyPicture;
        m_entityPictures[MapEntity::TERRAIN_MODIFICATION_6] = emptyPicture;
        m_entityPictures[MapEntity::TERRAIN_MODIFICATION_7] = emptyPicture;
        m_entityPictures[MapEntity::DROPPED_AMBER] = emptyPicture;
        m_entityPictures[MapEntity::DROPPED_BEECH_SEEDS] = emptyPicture;
        m_entityPictures[MapEntity::DROPPED_AMBER] = emptyPicture;
        m_entityPictures[MapEntity::DROPPED_BEECH_SEEDS] = emptyPicture;
        m_entityPictures[MapEntity::DROPPED_BOAR_TROPHY] = emptyPicture;
        m_entityPictures[MapEntity::DROPPED_BRONZE_NAILS] = emptyPicture;
        m_entityPictures[MapEntity::DROPPED_CHAIN] = emptyPicture;
        m_entityPictures[MapEntity::DROPPED_COAL] = emptyPicture;
        m_entityPictures[MapEntity::DROPPED_DANDELION] = emptyPicture;
        m_entityPictures[MapEntity::DROPPED_DEER_HIDE] = emptyPicture;
        m_entityPictures[MapEntity::DROPPED_DEER_TROPHY] = emptyPicture;
        m_entityPictures[MapEntity::DROPPED_ENTRAILS] = emptyPicture;
        m_entityPictures[MapEntity::DROPPED_FEATHERS] = emptyPicture;
        m_entityPictures[MapEntity::DROPPED_FINE_WOOD] = emptyPicture;
        m_entityPictures[MapEntity::DROPPED_FLINT] = emptyPicture;
        m_entityPictures[MapEntity::DROPPED_FLINTHEAD_ARROW] = emptyPicture;
        m_entityPictures[MapEntity::DROPPED_GREYDWARF_EYE] = emptyPicture;
        m_entityPictures[MapEntity::DROPPED_IRON_NAILS] = emptyPicture;
        m_entityPictures[MapEntity::DROPPED_LEATHER_SCRAPS] = emptyPicture;
        m_entityPictures[MapEntity::DROPPED_MUSHROOM] = emptyPicture;
        m_entityPictures[MapEntity::DROPPED_NECK_TAIL] = emptyPicture;
        m_entityPictures[MapEntity::DROPPED_OBSIDIAN] = emptyPicture;
        m_entityPictures[MapEntity::DROPPED_RAW_MEAT] = emptyPicture;
        m_entityPictures[MapEntity::DROPPED_RESIN] = emptyPicture;
        m_entityPictures[MapEntity::DROPPED_STONE] = emptyPicture;
        m_entityPictures[MapEntity::DROPPED_SURTLING_CORE_1] = emptyPicture;
        m_entityPictures[MapEntity::DROPPED_SURTLING_CORE_2] = emptyPicture;
        m_entityPictures[MapEntity::DROPPED_WOOD] = emptyPicture;
        m_entityPictures[MapEntity::SPAWN_BOAR_1] = emptyPicture;
        m_entityPictures[MapEntity::SPAWN_BOAR_2] = emptyPicture;
        m_entityPictures[MapEntity::SPAWN_DEER] = emptyPicture;
        m_entityPictures[MapEntity::SPAWN_FISH_1] = emptyPicture;
        m_entityPictures[MapEntity::SPAWN_FISH_2] = emptyPicture;
        m_entityPictures[MapEntity::SPAWN_FISH_3] = emptyPicture;
        m_entityPictures[MapEntity::SPAWN_GREYDWARF_1] = emptyPicture;
        m_entityPictures[MapEntity::SPAWN_GREYDWARF_2] = emptyPicture;
        m_entityPictures[MapEntity::SPAWN_GREYDWARF_BRUTE] = emptyPicture;
        m_entityPictures[MapEntity::SPAWN_GREYDWARF_SHAMAN] = emptyPicture;
        m_entityPictures[MapEntity::SPAWN_GREYLING] = emptyPicture;
        m_entityPictures[MapEntity::SPAWN_NECK] = emptyPicture;
        m_entityPictures[MapEntity::SPAWN_SKELETON] = emptyPicture;
        m_entityPictures[MapEntity::SPAWN_SURTLING_1] = emptyPicture;
        m_entityPictures[MapEntity::SPAWN_SURTLING_2] = emptyPicture;

        // color all known entities that we don't explicitly set a picture for
        // with a darker dot than unknown entities
        QPicture knownEntityPicture;
        painter.begin(&knownEntityPicture);
        painter.fillRect(QRectF(-0.5f, -0.5f, 1.0f, 1.0f), QColor(0x12, 0x4e, 0x89).darker(170));
        painter.end();
        for (auto it = MapEntity::TypeNames.keyBegin(), end = MapEntity::TypeNames.keyEnd();
             it != end; ++it) {
            if (!m_entityPictures.contains(*it)) {
                m_entityPictures[*it] = knownEntityPicture;
            }
        }
    }

    // structure pictures
    {
        painter.begin(&m_defaultStructurePicture);
        painter.fillRect(QRectF(-2.5f, -2.5f, 5.0f, 5.0f), QColor(0xb5, 0x50, 0x88, 0x88));
        painter.end();

        painter.begin(&m_structurePictures[MapStructure::START_TEMPLE]);
        painter.drawImage(iconPos, m_startTempleIcon);
        painter.end();

        painter.begin(&m_structurePictures[MapStructure::DRAUGR_VILLAGE]);
        painter.drawImage(iconPos, m_draugrVillageIcon);
        painter.end();

        painter.begin(&m_structurePictures[MapStructure::TROLL_CAVE]);
        painter.drawImage(iconPos, m_trollCaveIcon);
        painter.end();

        painter.begin(&m_structurePictures[MapStructure::SUNKEN_CRYPT]);
        painter.drawImage(iconPos, m_sunkenCryptIcon);
        painter.end();

        painter.begin(&m_structurePictures[MapStructure::BURIAL_CHAMBER]);
        painter.drawImage(iconPos, m_burialChamberIcon);
        painter.end();

        painter.begin(&m_structurePictures[MapStructure::FULING_VILLAGE]);
        painter.drawImage(iconPos, m_fulingVillageIcon);
        painter.end();

        painter.begin(&m_structurePictures[MapStructure::DRAKE_NEST]);
        painter.drawImage(iconPos, m_drakeNestIcon);
        painter.end();

        painter.begin(&m_structurePictures[MapStructure::HALDOR]);
        painter.drawImage(iconPos, m_haldorIcon);
        painter.end();

        painter.begin(&m_structurePictures[MapStructure::EIKTHYR]);
        painter.drawImage(iconPos, m_eikthyrIcon);
        painter.end();

        painter.begin(&m_structurePictures[MapStructure::THE_ELDER]);
        painter.drawImage(iconPos, m_theElderIcon);
        painter.end();

        painter.begin(&m_structurePictures[MapStructure::BONEMASS]);
        painter.drawImage(iconPos, m_bonemassIcon);
        painter.end();

        painter.begin(&m_structurePictures[MapStructure::MODER]);
        painter.drawImage(iconPos, m_moderIcon);
        painter.end();

        painter.begin(&m_structurePictures[MapStructure::YAGLUTH]);
        painter.drawImage(iconPos, m_yagluthIcon);
        painter.end();
    }
}

void MapView::setMap(QPointer<Map> map)
{
    if (m_map) {
        QObject::disconnect(m_map, nullptr, this, nullptr);
        clearSelection();
    }
    m_map = map;
    if (m_map) {
        connect(m_map, &Map::mapLoaded, this, [&]() {
            clearSelection();
            generateTerrainPictures();
            fitMap(false);
            update();
        });
        generateTerrainPictures();
        fitMap(false);
    }
    update();
    emit mapChanged(m_map);
}

void MapView::paint(QPainter *painter)
{
    if (m_map == nullptr) {
        return;
    }
    painter->setRenderHint(QPainter::Antialiasing, true);
    painter->scale(m_scale, m_scale);
    painter->translate(-m_xOffset, m_zOffset);

    const bool isOverview = m_scale < 2.0f;

    m_visibleRect = {m_xOffset - 32.0f, m_zOffset - (height() / m_scale) - 32.0f,
                     (width() / m_scale) + 64.0f, (height() / m_scale) + 64.0f};
    const QPair<float, float> yRange = getYRange();

    // always draw terrain underneath everything
    for (const MapEntity *entity : m_map->terrain()) {
        const QPoint pos = entity->flatPosition().toPoint();
        if (!m_visibleRect.contains(pos)) {
            continue;
        }
        QImage terrain = m_terrainPictures.value(entity->id()).at(0);
        QImage entities =
            m_terrainPictures.value(entity->id()).at(m_currentLayer == LAYER_WORLD ? 1 : 2);
        if (!terrain.isNull()) {
            painter->save();
            painter->drawImage(QRectF(pos.x() - 32.0f, -pos.y() - 32.0f, 64.0f, 64.0f), terrain);
            painter->restore();
        }
        if (!isOverview && !entities.isNull()) {
            painter->save();
            painter->drawImage(QRectF(pos.x() - 32.0f, -pos.y() - 32.0f, 64.0f, 64.0f), entities);
            painter->restore();
        }
    }

    // draw deposit icons over random entities but below portals and structures
    if (!isOverview) {
        for (const MapEntity *entity : m_map->deposits()) {
            const float y = entity->position().y();
            const QPointF pos = entity->flatPosition();
            if (yRange.first > y || yRange.second < y || !m_visibleRect.contains(pos)) {
                continue;
            }
            QPicture pic = picture(entity->type());
            if (!pic.isNull()) {
                painter->save();
                painter->translate(pos.x(), -pos.y());
                painter->scale(1.0f / m_scale, 1.0f / m_scale);
                pic.play(painter);
                painter->restore();
            }
        }
    }

    // draw structures below portals, since portals are important for navigation
    for (const MapStructure *structure : m_map->structures()) {
        const QPointF pos = structure->flatPosition();
        if (!structure->built() || !m_visibleRect.contains(pos) ||
            (m_currentLayer == LAYER_CRYPTS && !m_cryptTypes.contains(structure->type())) ||
            (isOverview && !m_overviewTypes.contains(structure->type()))) {
            continue;
        }
        QPicture pic = picture(structure->type());
        if (!pic.isNull()) {
            painter->save();
            painter->translate(pos.x(), -pos.y());
            if (pic.width() >= 24.0f) {
                painter->scale(1.0f / m_scale, 1.0f / m_scale);
            }
            pic.play(painter);
            painter->restore();
        }
    }

    // finally draw portals on top of everything else
    for (const MapEntity *entity : m_map->portals()) {
        const QPointF pos = entity->flatPosition();
        if (!m_visibleRect.contains(pos)) {
            continue;
        }
        QPicture pic = picture(entity->type());
        if (!pic.isNull()) {
            painter->save();
            painter->translate(pos.x(), -pos.y());
            painter->scale(1.0f / m_scale, 1.0f / m_scale);
            pic.play(painter);
            painter->restore();
        }
    }

    // temporarily draw mystery entities so i can find em!
    //    for (const MapEntity *entity : m_map->enities()) {
    //        if (!entity->table7().isEmpty()) {
    //            const QPointF pos = entity->flatPosition();
    //            QPicture pic = picture(MapEntity::MYSTERY);
    //            painter->save();
    //            painter->translate(pos.x(), -pos.y());
    //            painter->rotate(entity->rotation().y());
    //            pic.play(painter);
    //            painter->restore();
    //        }
    //    }

    if (m_selectionRect.isValid()) {
        painter->setPen(Qt::NoPen);
        painter->setBrush(QColor(0x63, 0xc7, 0x4d, 0x66));
        painter->drawRect(QRectF(m_selectionRect.x(),
                                 -(m_selectionRect.y() + m_selectionRect.height()),
                                 m_selectionRect.width(), m_selectionRect.height()));
    }
}

void MapView::select(const float mouseX, const float mouseY)
{
    if (mapValid()) {
        m_entities.clear();
        m_structures.clear();
        const float selectionSize = qMax(1.0f, 20.0f / m_scale);
        m_selectionRect = {m_xOffset + ((mouseX - 1.5f) / m_scale) - (selectionSize / 2.0f),
                           m_zOffset - ((mouseY - 3.0f) / m_scale) - (selectionSize / 2.0f),
                           selectionSize, selectionSize};
        for (MapEntity *entity : m_map->portals()) {
            if (!m_selectionRect.contains(entity->flatPosition())) {
                continue;
            }
            m_entities.append(entity);
        }
        for (MapEntity *entity : m_map->deposits()) {
            if ((m_currentLayer == LAYER_WORLD && entity->position().y() > 5000) ||
                (m_currentLayer == LAYER_CRYPTS && entity->position().y() < 5000) ||
                !m_selectionRect.contains(entity->flatPosition())) {
                continue;
            }
            m_entities.append(entity);
        }
        for (MapEntity *entity : m_map->terrain()) {
            if (!m_selectionRect.contains(entity->flatPosition())) {
                continue;
            }
            m_entities.append(entity);
        }
        for (MapEntity *entity : m_map->enities()) {
            if ((m_currentLayer == LAYER_WORLD && entity->position().y() > 5000) ||
                (m_currentLayer == LAYER_CRYPTS && entity->position().y() < 5000) ||
                !m_selectionRect.contains(entity->flatPosition()) ||
                (!m_showAll && picture(entity->type()).isNull())) {
                continue;
            }
            m_entities.append(entity);
        }
        for (MapStructure *structure : m_map->structures()) {
            if (structure->built() && m_selectionRect.contains(structure->flatPosition())) {
                m_structures.append(structure);
            }
        }
        update();
        emit selectionChanged();
    }
}

void MapView::zoomIn(const QPointF &around)
{
    if (mapValid() && m_scale <= 40.f) {
        m_scale *= 1.1f;
        m_xOffset += (around.x() * 0.1f) / m_scale;
        m_zOffset -= (around.y() * 0.1f) / m_scale;
        update();
        emit mapScaleChanged(m_scale);
        emit offsetChanged();
    }
}

void MapView::zoomOut(const QPointF &around)
{
    if (mapValid() && m_scale >= 0.03f) {
        m_scale *= 0.9f;
        m_xOffset -= (around.x() * 0.1f) / m_scale;
        m_zOffset += (around.y() * 0.1f) / m_scale;
        update();
        emit mapScaleChanged(m_scale);
        emit offsetChanged();
    }
}

void MapView::scroll(const QPointF &mouseOffset)
{
    if (mapValid()) {
        m_xOffset -= mouseOffset.x() / m_scale;
        m_zOffset += mouseOffset.y() / m_scale;
        update();
        emit offsetChanged();
    }
}

void MapView::fitMap(bool callUpdate)
{
    if (mapValid()) {
        const float xSize = (m_map->maxPosition().x() - m_map->minPosition().x());
        const float zSize = (m_map->maxPosition().z() - m_map->minPosition().z());
        const float xScale = (width() - 10.0f) / xSize;
        const float zScale = (height() - 10.0f) / zSize;
        m_scale = qMin(xScale, zScale);
        m_xOffset = m_map->minPosition().x() - ((((width() / m_scale) - xSize)) / 2.0f);
        m_zOffset = m_map->maxPosition().z() + ((((height() / m_scale) - zSize)) / 2.0f);
        m_currentLayer = LAYER_WORLD;
        m_showAll = false;
        if (callUpdate)
            update();
        emit mapScaleChanged(m_scale);
        emit offsetChanged();
        emit currentLayerChanged(m_currentLayer);
    }
}

void MapView::zoomToPoint(const QPointF &point, const float &scale)
{
    if (mapValid()) {
        m_scale = scale;
        m_xOffset = point.x() - ((width() / 2.0f) / m_scale);
        m_zOffset = point.y() + ((height() / 2.0f) / m_scale);
        m_currentLayer = LAYER_WORLD;
        update();
        emit offsetChanged();
        emit currentLayerChanged(m_currentLayer);
    }
}

void MapView::zoomToSpawn()
{
    if (mapValid()) {
        QPointF spawnPosition;
        for (const MapStructure *structure : m_map->structures()) {
            if (structure->type() == MapStructure::START_TEMPLE) {
                spawnPosition = structure->flatPosition();
                break;
            }
        }
        zoomToPoint(spawnPosition, 4.5f);
    }
}

void MapView::generateTerrainPictures()
{
    m_terrainPictures.clear();
    const QMap<QPair<qint32, qint32>, QList<MapEntity *>> &chunks = m_map->chunks();
    QPainter painter1, painter2, painter3;
    for (const MapEntity *terrain : m_map->terrain()) {
        QImage first(512, 512, QImage::Format_RGB32);
        QImage second(512, 512, QImage::Format_ARGB32_Premultiplied);
        second.fill(Qt::transparent);
        QImage third(512, 512, QImage::Format_ARGB32_Premultiplied);
        third.fill(Qt::transparent);

        // draw terrain background on first picture, for overview
        painter1.begin(&first);
        painter1.setRenderHint(QPainter::Antialiasing, true);
        painter1.scale(8, 8);
        painter1.translate(32, 32);
        painter1.setPen(Qt::NoPen);
        if (terrain->allMatch()) {
            // terrain is all one biome
            painter1.fillRect(QRect(-32, -32, 64, 64), BiomeColors[terrain->nwBiome()]);
        } else if (terrain->topMatch() && terrain->bottomMatch() &&
                   !(terrain->leftMatch() || terrain->rightMatch())) {
            // terrain has a horizontal biome border through it
            painter1.fillRect(QRect(-32, -32, 64, 32), BiomeColors[terrain->nwBiome()]);
            painter1.fillRect(QRect(-32, 0, 64, 32), BiomeColors[terrain->swBiome()]);
        } else if (terrain->leftMatch() && terrain->rightMatch() &&
                   !(terrain->topMatch() || terrain->bottomMatch())) {
            // terrain has a vertical biome border through it
            painter1.fillRect(QRect(-32, -32, 32, 64), BiomeColors[terrain->nwBiome()]);
            painter1.fillRect(QRect(0, -32, 32, 64), BiomeColors[terrain->neBiome()]);
        } else if (terrain->topMatch() && terrain->leftMatch()) {
            // have NW, NE, and SW matching
            painter1.fillRect(QRect(-32, -32, 64, 64), BiomeColors[terrain->nwBiome()]);
            QPainterPath path;
            path.moveTo(32, 0);
            path.arcTo(0, 0, 64, 64, 180, -90);
            path.lineTo(32, 32);
            path.lineTo(0, 32);
            painter1.setBrush(BiomeColors[terrain->seBiome()]);
            painter1.drawPath(path);
        } else if (terrain->topMatch() && terrain->rightMatch()) {
            // have NW, NE, and SE matching
            painter1.fillRect(QRect(-32, -32, 64, 64), BiomeColors[terrain->nwBiome()]);
            QPainterPath path;
            path.moveTo(-32, 0);
            path.arcTo(-64, 0, 64, 64, 0, 90);
            path.lineTo(-32, 32);
            path.lineTo(0, 32);
            painter1.setBrush(BiomeColors[terrain->swBiome()]);
            painter1.drawPath(path);
        } else if (terrain->bottomMatch() && terrain->leftMatch()) {
            // have SW, SE, and NW matching
            painter1.fillRect(QRect(-32, -32, 64, 64), BiomeColors[terrain->swBiome()]);
            QPainterPath path;
            path.moveTo(32, 0);
            path.arcTo(0, -64, 64, 64, 180, 90);
            path.lineTo(32, -32);
            path.lineTo(0, -32);
            painter1.setBrush(BiomeColors[terrain->neBiome()]);
            painter1.drawPath(path);
        } else if (terrain->bottomMatch() && terrain->rightMatch()) {
            // have SW, SE, and NE matching
            painter1.fillRect(QRect(-32, -32, 64, 64), BiomeColors[terrain->swBiome()]);
            QPainterPath path;
            path.moveTo(0, -32);
            path.arcTo(-64, -64, 64, 64, -90, 90);
            path.lineTo(-32, -32);
            path.lineTo(-32, 0);
            painter1.setBrush(BiomeColors[terrain->nwBiome()]);
            painter1.drawPath(path);
        } else if (terrain->topMatch() && !terrain->bottomMatch()) {
            painter1.fillRect(QRect(-32, -32, 64, 64), BiomeColors[terrain->nwBiome()]);
            QPainterPath path;
            path.moveTo(-32, 0);
            path.arcTo(-64, 0, 64, 16, 0, 90);
            path.lineTo(-32, 32);
            path.lineTo(0, 32);
            path.lineTo(0, 8);
            painter1.setBrush(BiomeColors[terrain->swBiome()]);
            painter1.drawPath(path);
            path.clear();
            path.moveTo(32, 0);
            path.arcTo(0, 0, 64, 16, 180, -90);
            path.lineTo(32, 32);
            path.lineTo(0, 32);
            path.lineTo(0, 8);
            painter1.setBrush(BiomeColors[terrain->seBiome()]);
            painter1.drawPath(path);
        } else if (terrain->bottomMatch() && !terrain->topMatch()) {
            painter1.fillRect(QRect(-32, -32, 64, 64), BiomeColors[terrain->swBiome()]);
            QPainterPath path;
            path.moveTo(-32, 0);
            path.arcTo(-64, -16, 64, 16, 0, -90);
            path.lineTo(-32, -32);
            path.lineTo(0, -32);
            path.lineTo(0, -8);
            painter1.setBrush(BiomeColors[terrain->nwBiome()]);
            painter1.drawPath(path);
            path.clear();
            path.moveTo(32, 0);
            path.arcTo(0, -16, 64, 16, 180, 90);
            path.lineTo(32, -32);
            path.lineTo(0, -32);
            path.lineTo(0, -8);
            painter1.setBrush(BiomeColors[terrain->neBiome()]);
            painter1.drawPath(path);
        } else if (terrain->leftMatch() && !terrain->rightMatch()) {
            painter1.fillRect(QRect(-32, -32, 64, 64), BiomeColors[terrain->nwBiome()]);
            QPainterPath path;
            path.moveTo(0, -32);
            path.arcTo(0, -64, 16, 64, 180, 90);
            path.lineTo(32, 0);
            path.lineTo(32, -32);
            path.lineTo(0, -32);
            painter1.setBrush(BiomeColors[terrain->neBiome()]);
            painter1.drawPath(path);
            path.clear();
            path.moveTo(0, 32);
            path.arcTo(0, 0, 16, 64, 180, -90);
            path.lineTo(32, 0);
            path.lineTo(32, 32);
            path.lineTo(0, 32);
            painter1.setBrush(BiomeColors[terrain->seBiome()]);
            painter1.drawPath(path);
        } else if (terrain->rightMatch() && !terrain->leftMatch()) {
            painter1.fillRect(QRect(-32, -32, 64, 64), BiomeColors[terrain->neBiome()]);
            QPainterPath path;
            path.moveTo(0, -32);
            path.arcTo(-16, -64, 16, 64, 0, -90);
            path.lineTo(-32, 0);
            path.lineTo(-32, -32);
            path.lineTo(0, -32);
            painter1.setBrush(BiomeColors[terrain->nwBiome()]);
            painter1.drawPath(path);
            path.clear();
            path.moveTo(0, 32);
            path.arcTo(-16, 0, 16, 64, 0, 90);
            path.lineTo(-32, 0);
            path.lineTo(-32, 32);
            path.lineTo(0, 32);
            painter1.setBrush(BiomeColors[terrain->swBiome()]);
            painter1.drawPath(path);
        } else if (terrain->neBiome() == terrain->swBiome()) {
            painter1.fillRect(QRect(-32, -32, 64, 64), BiomeColors[terrain->neBiome()]);
            QPainterPath path;
            path.moveTo(0, -32);
            path.arcTo(-4, -4, 4, 4, 0, -90);
            path.lineTo(-32, 0);
            path.lineTo(-32, -32);
            painter1.setBrush(BiomeColors[terrain->nwBiome()]);
            painter1.drawPath(path);
            path.clear();
            path.moveTo(32, 0);
            path.arcTo(0, 0, 4, 4, 90, 90);
            path.lineTo(0, 32);
            path.lineTo(32, 32);
            painter1.setBrush(BiomeColors[terrain->seBiome()]);
            painter1.drawPath(path);
        } else if (terrain->nwBiome() == terrain->seBiome()) {
            painter1.fillRect(QRect(-32, -32, 64, 64), BiomeColors[terrain->nwBiome()]);
            QPainterPath path;
            path.moveTo(-32, 0);
            path.arcTo(-64, 0, 64, 64, 0, 90);
            path.lineTo(-32, 32);
            path.lineTo(0, 32);
            painter1.setBrush(BiomeColors[terrain->swBiome()]);
            painter1.drawPath(path);
            path.clear();
            path.moveTo(32, 0);
            path.arcTo(0, -64, 64, 64, 180, 90);
            path.lineTo(32, -32);
            path.lineTo(0, -32);
            painter1.setBrush(BiomeColors[terrain->neBiome()]);
            painter1.drawPath(path);
        } else {
            painter1.fillRect(QRect(-32, -32, 32, 32), BiomeColors[terrain->nwBiome()]);
            painter1.fillRect(QRect(-32, 0, 32, 32), BiomeColors[terrain->swBiome()]);
            painter1.fillRect(QRect(0, -32, 32, 32), BiomeColors[terrain->neBiome()]);
            painter1.fillRect(QRect(0, 0, 32, 32), BiomeColors[terrain->seBiome()]);
        }
        painter1.setBrush(Qt::NoBrush);
        painter1.end();

        // draw entities near terrain block on second and third picture for
        // the close up world and crypt layers
        painter2.begin(&second);
        painter2.setRenderHint(QPainter::Antialiasing, true);
        painter2.scale(8, 8);
        painter2.translate(32, 32);
        painter3.begin(&third);
        painter3.setRenderHint(QPainter::Antialiasing, true);
        painter3.scale(8, 8);
        painter3.translate(32, 32);

        painter2.setPen(
            {QColor(0x26, 0x25, 0x44, 0x22), 0.75f, Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin});
        painter2.setFont(QFont("Roboto", 4));
        painter2.drawRect(-32, -32, 64, 64);
        painter2.drawText(-30, 29,
                          QString("(%1, %2)").arg(terrain->chunkX()).arg(terrain->chunkY()));
        painter3.setPen(
            {QColor(0x26, 0x25, 0x44, 0x22), 0.75f, Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin});
        painter3.setFont(QFont("Roboto", 4));
        painter3.drawRect(-32, -32, 64, 64);
        painter3.drawText(-30, 29,
                          QString("(%1, %2)").arg(terrain->chunkX()).arg(terrain->chunkY()));

        const QPointF terrainPos = terrain->flatPosition();
        for (const MapEntity *entity : chunks[terrain->chunk()]) {
            QPicture pic = picture(entity->type());
            if (pic.isNull()) {
                continue;
            }
            const float y = entity->position().y();
            const QPointF pos = entity->flatPosition();
            if (y > -500.0f && y < 500.0f) {
                // draw world layer entities on second picture
                painter2.save();
                painter2.translate(pos.x() - terrainPos.x(), terrainPos.y() - pos.y());
                painter2.rotate(entity->rotation().y());
                pic.play(&painter2);
                painter2.restore();
            } else if (y > 5000.0f && y < 5500.0f) {
                // draw crypt layer entities on third picture
                painter3.save();
                painter3.translate(pos.x() - terrainPos.x(), terrainPos.y() - pos.y());
                painter3.rotate(entity->rotation().y());
                pic.play(&painter3);
                painter3.restore();
            }
        }
        painter2.end();
        painter3.end();

        m_terrainPictures[terrain->id()].append(first);
        m_terrainPictures[terrain->id()].append(second);
        m_terrainPictures[terrain->id()].append(third);
    }
}
