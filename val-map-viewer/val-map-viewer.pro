TEMPLATE = app
TARGET = "Valheim Map Viewer"
QT += quick quickcontrols2
CONFIG += c++17
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp \
        map.cpp \
        mapentity.cpp \
        mapstructure.cpp \
        mapview.cpp \
        valheim.cpp

RESOURCES += \
    resources.qrc

HEADERS += \
    asm64.h \
    dotnet.h \
    map.h \
    mapentity.h \
    mapstructure.h \
    mapview.h \
    valheim.h

macx {
    # sdk_no_version_check can be removed once Qt releases an update for macOS 11
    CONFIG += sdk_no_version_check
    ICON = resources/icon.icns
}

win32 {
    RC_ICONS = resources/icon.ico
}
