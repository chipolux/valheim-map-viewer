#include "mapentity.h"

#include <QDataStream>
#include <QDebug>
#include <QIODevice>
#include <bitset>

#include "dotnet.h"

MapEntity::MapEntity(const QByteArray &data, const qint64 &offset, const qint64 &userId,
                     const quint32 &id, const quint32 &mapVersion, QObject *parent)
    : QObject(parent)
    , m_offset(offset)
    , m_userId(userId)
    , m_id(id)
{
    QDataStream stream(data);
    stream.setByteOrder(QDataStream::LittleEndian);
    stream.setFloatingPointPrecision(QDataStream::SinglePrecision);

    stream >> m_ownerRevision;
    stream >> m_dataRevision;
    stream >> m_persistent;
    stream >> m_owner;
    stream >> m_timeCreated;
    stream >> m_pgwVersion;
    if (mapVersion >= 16 && mapVersion < 24) {
        stream.skipRawData(4); // read qint32 but not stored
    }
    if (mapVersion >= 23) {
        stream >> m_objectType; // coerce from qint8 to ZDO.ObjectType
    }
    if (mapVersion >= 22) {
        stream >> m_distant;
    }
    if (mapVersion < 13) {
        stream.skipRawData(2); // read 2 char, coerce to int, but never use
    }
    if (mapVersion >= 17) {
        stream >> m_type; // prefab
    }
    // read Vector2i (sector)
    stream >> m_chunk.first;
    stream >> m_chunk.second;
    // read Vector3 (position)
    stream >> m_xCoord;
    stream >> m_yCoord;
    stream >> m_zCoord;
    // read Quaternion (rotation)
    stream >> m_xRot;
    stream >> m_yRot;
    stream >> m_zRot;
    stream >> m_wRot;
    QQuaternion(m_wRot, m_xRot, m_yRot, m_zRot).getEulerAngles(&m_xRotEul, &m_yRotEul, &m_zRotEul);
    // table 1 is floats
    {
        char tableSize;
        stream >> tableSize;
        if (tableSize > 0) {
            for (char i = 0; i < tableSize; i++) {
                qint32 key;
                stream >> key;
                stream >> m_floats[key];
            }
        }
    }
    // table 2 is vector3s
    {
        char tableSize;
        stream >> tableSize;
        if (tableSize > 0) {
            for (char i = 0; i < tableSize; i++) {
                qint32 key;
                float x, y, z;
                stream >> key >> x >> y >> z;
                m_vecs[key] = {x, y, z};
            }
        }
    }
    // table 3 is quaternions
    {
        char tableSize;
        stream >> tableSize;
        if (tableSize > 0) {
            for (char i = 0; i < tableSize; i++) {
                qint32 key;
                float x, y, z, w;
                stream >> key >> x >> y >> z >> w;
                m_quats[key] = {w, x, y, z};
            }
        }
    }
    // table 4 is 32 bit integers
    {
        char tableSize;
        stream >> tableSize;
        if (tableSize > 0) {
            for (char i = 0; i < tableSize; i++) {
                qint32 key, value;
                stream >> key >> value;
                m_ints[key] = value;
            }
        }
    }
    // table 5 is 64 bit integers
    {
        char tableSize;
        stream >> tableSize;
        if (tableSize > 0) {
            for (char i = 0; i < tableSize; i++) {
                qint32 key;
                qint64 value;
                stream >> key >> value;
                m_longs[key] = value;
            }
        }
    }
    // table 6 is variable length strings with ULEB length prefixes
    {
        char tableSize;
        stream >> tableSize;
        if (tableSize > 0) {
            for (char i = 0; i < tableSize; i++) {
                qint32 key;
                stream >> key;
                m_strings.insert(key, dotnet::readString(stream));
            }
        }
    }
    // table 7 was added in map file version 27
    // contains raw byte values
    if (mapVersion >= 27) {
        char tableSize;
        stream >> tableSize;
        if (tableSize > 0) {
            for (char i = 0; i < tableSize; i++) {
                qint32 key, length;
                stream >> key >> length;
                if (length > 0) {
                    char *buf = new char[length];
                    stream.readRawData(buf, length);
                    m_byteArrays.insert(key, QByteArray(buf, length));
                    delete[] buf;
                }
            }
        }
    }
}
