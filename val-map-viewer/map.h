#ifndef MAP_H
#define MAP_H

#include <QDir>
#include <QObject>
#include <QPicture>
#include <QStandardPaths>
#include <QUrl>
#include <QVector3D>

#include "mapentity.h"
#include "mapstructure.h"
#include "valheim.h"

class Map : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool loaded READ loaded NOTIFY mapLoaded)
    Q_PROPERTY(QString name READ name NOTIFY mapLoaded)
    Q_PROPERTY(QString seed READ seed NOTIFY mapLoaded)
    Q_PROPERTY(QUrl mapDataFolder READ mapDataFolder CONSTANT)

  public:
    explicit Map(QObject *parent = nullptr);

    const bool &loaded() const { return m_loaded; }
    const QString &name() const { return m_mapName; }
    const QString &seed() const { return m_seedName; }
    const QVector3D &maxPosition() const { return qAsConst(m_maxPosition); }
    const QVector3D &minPosition() const { return qAsConst(m_minPosition); }
    const QList<MapEntity *> &terrain() const { return qAsConst(m_terrain); }
    const MapEntity *terrain(int index) const { return m_terrain.value(index); }
    const QList<MapEntity *> &enities() const { return qAsConst(m_entities); }
    const QList<MapEntity *> &deposits() const { return qAsConst(m_deposits); }
    const QList<MapEntity *> &portals() const { return qAsConst(m_portals); }
    const QList<MapStructure *> &structures() const { return qAsConst(m_structures); }
    const QUrl mapDataFolder() const;
    const QString getRawEntity(const quint32 &offset) const;
    Valheim::Biome getBiome(float x, float z) const { return m_valheim->getBiome(x, z); }
    Valheim::Biome getBiome(const QPointF &point) const { return getBiome(point.x(), point.y()); }
    const QMap<QPair<qint32, qint32>, QList<MapEntity *>> &chunks() const
    {
        return m_entitiesByChunk;
    }

  signals:
    void mapLoaded(QString error);

  public slots:
    void loadMap(QUrl filePath);

  private:
    Valheim *m_valheim;
    QString loadEntities(QDataStream &stream);
    QString loadStructures(QDataStream &stream);
    void postLoad() const;

    bool m_loaded;
    QUrl m_mapPath;
    QString m_mapName;
    QString m_seedName;
    qint32 m_seed;
    qint32 m_mapVersion;
    double m_netTime;
    QList<MapEntity *> m_terrain;
    QList<MapEntity *> m_entities;
    QList<MapEntity *> m_deposits;
    QList<MapEntity *> m_portals;
    QList<MapStructure *> m_structures;
    QMap<QPair<qint32, qint32>, QList<MapEntity *>> m_entitiesByChunk;
    QVector3D m_maxPosition;
    QVector3D m_minPosition;
};

#endif // MAP_H
