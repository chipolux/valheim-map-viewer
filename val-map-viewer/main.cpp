#include <QDateTime>
#include <QFontDatabase>
#include <QGuiApplication>
#include <QIcon>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickStyle>

#include "map.h"
#include "mapview.h"
#include "valheim.h"

void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    QByteArray localMsg = msg.toLocal8Bit();
    const char *file = context.file ? context.file : "";
    const char *function = context.function ? context.function : "";
    QByteArray time = QDateTime::currentDateTime().toString(Qt::ISODateWithMs).toLatin1();
    switch (type) {
    case QtDebugMsg:
#ifdef QT_DEBUG
        fprintf(stderr, "[%s] DBUG: %s\n", time.constData(), localMsg.constData());
        break;
#endif
    case QtInfoMsg:
        fprintf(stderr, "[%s] INFO: %s\n", time.constData(), localMsg.constData());
        break;
    case QtWarningMsg:
        fprintf(stderr, "[%s] WARN: %s (%s:%u, %s)\n", time.constData(), localMsg.constData(), file,
                context.line, function);
        break;
    case QtCriticalMsg:
        fprintf(stderr, "[%s] CRIT: %s (%s:%u, %s)\n", time.constData(), localMsg.constData(), file,
                context.line, function);
        break;
    case QtFatalMsg:
        fprintf(stderr, "[%s] FATL: %s (%s:%u, %s)\n", time.constData(), localMsg.constData(), file,
                context.line, function);
        abort();
    }
}

bool registerRoboto()
{
    bool success = true;
    if (QFontDatabase::addApplicationFont(":/resources/Roboto/Roboto-Black.ttf") == -1)
        success = false;
    if (QFontDatabase::addApplicationFont(":/resources/Roboto/Roboto-BlackItalic.ttf") == -1)
        success = false;
    if (QFontDatabase::addApplicationFont(":/resources/Roboto/Roboto-Bold.ttf") == -1)
        success = false;
    if (QFontDatabase::addApplicationFont(":/resources/Roboto/Roboto-BoldItalic.ttf") == -1)
        success = false;
    if (QFontDatabase::addApplicationFont(":/resources/Roboto/Roboto-Italic.ttf") == -1)
        success = false;
    if (QFontDatabase::addApplicationFont(":/resources/Roboto/Roboto-Light.ttf") == -1)
        success = false;
    if (QFontDatabase::addApplicationFont(":/resources/Roboto/Roboto-LightItalic.ttf") == -1)
        success = false;
    if (QFontDatabase::addApplicationFont(":/resources/Roboto/Roboto-Medium.ttf") == -1)
        success = false;
    if (QFontDatabase::addApplicationFont(":/resources/Roboto/Roboto-MediumItalic.ttf") == -1)
        success = false;
    if (QFontDatabase::addApplicationFont(":/resources/Roboto/Roboto-Regular.ttf") == -1)
        success = false;
    if (QFontDatabase::addApplicationFont(":/resources/Roboto/Roboto-Thin.ttf") == -1)
        success = false;
    if (QFontDatabase::addApplicationFont(":/resources/Roboto/Roboto-ThinItalic.ttf") == -1)
        success = false;
    return success;
}

int main(int argc, char *argv[])
{
    qInstallMessageHandler(messageHandler);
    qDebug() << "Starting up!";

    qmlRegisterType<MapView>("Valheim", 1, 0, "MapView");
    qmlRegisterUncreatableType<MapEntity>("Valheim", 1, 0, "MapEntity", "Backend only.");
    qmlRegisterUncreatableType<MapStructure>("Valheim", 1, 0, "MapStructure", "Backend only.");
    qmlRegisterUncreatableType<Valheim>("Valheim", 1, 0, "Valheim", "Backend only.");

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);
    app.setApplicationDisplayName("Valheim Map Viewer");
    app.setWindowIcon(QIcon(":/resources/icon.png"));
    QQuickStyle::setStyle("Basic");
    if (!registerRoboto()) {
        qWarning() << "Failed to load Roboto!";
    } else {
        app.setFont({"Roboto", 13, QFont::Normal});
    }

    Map map(&app);

    QQmlApplicationEngine engine(&map);
    QQmlContext *ctx = engine.rootContext();
    ctx->setContextProperty("mapData", &map);

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(
        &engine, &QQmlApplicationEngine::objectCreated, &app,
        [url](QObject *obj, const QUrl &objUrl) {
            if (!obj && url == objUrl)
                QCoreApplication::exit(-1);
        },
        Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
