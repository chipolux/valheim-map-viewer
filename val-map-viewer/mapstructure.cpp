#include "mapstructure.h"

#include <QDataStream>
#include <QIODevice>

MapStructure::MapStructure(QDataStream &stream, QObject *parent)
    : QObject(parent)
{
    m_offset = stream.device()->pos();
    quint8 labelLength = 0;
    stream >> labelLength;
    char *label = new char[labelLength];
    stream.readRawData(label, labelLength);
    m_label = QString::fromLatin1(label, labelLength);
    delete[] label;
    stream >> m_xCoord;
    stream >> m_yCoord;
    stream >> m_zCoord;
    stream >> m_built;

    if (m_label == "StartTemple") {
        m_type = START_TEMPLE;
    } else if (m_label == "WoodVillage1") {
        m_type = DRAUGR_VILLAGE;
    } else if (m_label == "TrollCave02") {
        m_type = TROLL_CAVE;
    } else if (m_label == "SunkenCrypt4") {
        m_type = SUNKEN_CRYPT;
    } else if (m_label == "GoblinCamp2") {
        m_type = FULING_VILLAGE;
    } else if (m_label == "DrakeNest01") {
        m_type = DRAKE_NEST;
    } else if (m_label == "Vendor_BlackForest") {
        m_type = HALDOR;
    } else if (m_label == "Eikthyrnir") {
        m_type = EIKTHYR;
    } else if (m_label == "GDKing") {
        m_type = THE_ELDER;
    } else if (m_label == "Bonemass") {
        m_type = BONEMASS;
    } else if (m_label == "Dragonqueen") {
        m_type = MODER;
    } else if (m_label == "GoblinKing") {
        m_type = YAGLUTH;
    } else if (m_label.startsWith("Crypt")) {
        m_type = BURIAL_CHAMBER;
    } else {
        m_type = OTHER;
    }
}
