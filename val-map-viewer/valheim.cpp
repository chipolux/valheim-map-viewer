#include "valheim.h"

#include <QVector2D>
#include <QtDebug>
#include <QtMath>

#include "asm64.h"

Valheim::Valheim(QObject *parent)
    : QObject(parent)
    , m_offset0(0)
    , m_offset1(0)
    , m_offset2(0)
    , m_offset3(0)
    , m_offset4(0)
    , m_riverSeed(0)
    , m_streamSeed(0)
    , m_minMountainDistance(1000.0f)
{
}

void Valheim::initState(int32_t seed)
{
    m_state.a = seed;
    m_state.b = seed * 1812433253 + 1;
    m_state.c = m_state.b * 1812433253 + 1;
    m_state.d = m_state.c * 1812433253 + 1;
}

void Valheim::incrementState()
{
    const uint32_t s = m_state.d;
    uint32_t t = m_state.a;
    m_state.a = m_state.b;
    m_state.b = m_state.c;
    m_state.c = s;
    t ^= t << 11;
    t ^= t >> 8;
    m_state.d = t ^ s ^ (s >> 19);
}

float Valheim::range(float minInclusive, float maxInclusive)
{
    float value = getValue();
    minInclusive *= value;
    maxInclusive *= 1.0f - value;
    return minInclusive + maxInclusive;
}

int32_t Valheim::range(int32_t minInclusive, int32_t maxInclusive)
{
    if (minInclusive < maxInclusive) {
        incrementState();
        minInclusive = minInclusive + m_state.d % uint32_t(maxInclusive - minInclusive);
    } else if (maxInclusive < minInclusive) {
        incrementState();
        minInclusive = minInclusive - m_state.d % uint32_t(minInclusive - maxInclusive);
    }
    return minInclusive;
}

float Valheim::getValue()
{
    incrementState();
    return float((m_state.d & 0x7fffff) * 1.192093e-07);
}

float Valheim::perlinNoise(float x, float y) const
{
    asm64::Reg32 rax, rcx, rsi, rdi;
    asm64::XmmReg xmm0, xmm1, xmm2, xmm3, xmm4, xmm5, xmm6, xmm7, xmm8, xmm9, xmm10, xmm11, xmm12,
        xmm13, xmm14, xmm15;

    xmm0 = {x, 0.0f, 0.0f, 0.0f};
    xmm1 = {y, 0.0f, 0.0f, 0.0f};
    xmm10 = {-0.0f, -0.0f, -0.0f, -0.0f};
    asm64::movaps(xmm0, xmm2);
    asm64::xorps(xmm10, xmm2);
    asm64::xorps(xmm3, xmm3);
    asm64::movaps(xmm0, xmm7);
    asm64::cmpltss(xmm3, xmm7);
    asm64::andps(xmm7, xmm2);
    asm64::andnps(xmm0, xmm7);
    asm64::orps(xmm2, xmm7);
    asm64::movaps(xmm1, xmm0);
    asm64::xorps(xmm10, xmm0);
    asm64::movaps(xmm1, xmm11);
    asm64::cmpltss(xmm3, xmm11);
    asm64::andps(xmm11, xmm0);
    asm64::andnps(xmm1, xmm11);
    asm64::orps(xmm0, xmm11);
    rax.i = qFloor(xmm7.a.f);
    rcx.i = qFloor(xmm11.a.f);
    xmm0 = {float(rax.i), 0.0f, 0.0f, 0.0f};
    rax.i &= 0xff;
    xmm1 = {float(rcx.i), 0.0f, 0.0f, 0.0f};
    rcx.i &= 0xff;
    asm64::subss(xmm0, xmm7);
    asm64::subss(xmm1, xmm11);
    xmm12 = {1.0f, 0.0f, 0.0f, 0.0f};
    asm64::movaps(xmm12, xmm15);
    asm64::minss(xmm7, xmm15);
    rsi.i = permutations[rax.i];
    rsi.i += rcx.i;
    rdi.i = permutations[rsi.i];
    rsi.i = permutations[rsi.i + 1];
    xmm2 = {-1.0f, 0.0f, 0.0f, 0.0f};
    asm64::movsldup(xmm7, xmm8);
    asm64::addss(xmm2, xmm7);
    asm64::addss(xmm11, xmm2);
    xmm4 = {permutations[rsi.i], permutations[rdi.i], 0, 0};
    xmm6 = {15, 15, 0, 0};
    asm64::movdqa(xmm4, xmm3);
    asm64::pand(xmm6, xmm3);
    xmm14 = {8, 8, 0, 0};
    asm64::movdqa(xmm3, xmm0);
    asm64::pmaxud(xmm14, xmm0);
    asm64::pcmpeqd(xmm3, xmm0);
    xmm2.b.f = xmm11.a.f; // insertps $0x10
    xmm9 = {4, 4, 0, 0};
    asm64::movdqa(xmm3, xmm13);
    asm64::pmaxud(xmm9, xmm13);
    asm64::pcmpeqd(xmm3, xmm13);
    asm64::por({2, 2, 0, 0}, xmm3);
    asm64::pcmpeqd({14, 14, 0, 0}, xmm3);
    asm64::pand(xmm8, xmm3);
    asm64::blendvps(xmm0, xmm2, xmm8);
    asm64::movaps(xmm2, xmm5);
    asm64::movdqa(xmm13, xmm0);
    asm64::blendvps(xmm0, xmm3, xmm5);
    xmm3 = {1, 1, 0, 0};
    asm64::movdqa(xmm4, xmm0);
    asm64::pand(xmm3, xmm0);
    asm64::xorps(xmm1, xmm1);
    asm64::pcmpeqd(xmm1, xmm0);
    asm64::movaps(xmm8, xmm13);
    asm64::xorps(xmm10, xmm13);
    asm64::blendvps(xmm0, xmm8, xmm13);
    xmm8 = {2, 2, 0, 0};
    asm64::pand(xmm8, xmm4);
    asm64::pcmpeqd(xmm1, xmm4);
    asm64::movaps(xmm5, xmm1);
    asm64::xorps(xmm10, xmm1);
    asm64::movdqa(xmm4, xmm0);
    asm64::blendvps(xmm0, xmm5, xmm1);
    rax.i = permutations[rax.i + 1];
    rax.i += rcx.i;
    rcx.i = permutations[rax.i];
    rax.i = permutations[rax.i + 1];
    xmm4 = {permutations[rax.i], permutations[rcx.i], 0, 0};
    asm64::pand(xmm4, xmm6);
    asm64::pmaxud(xmm6, xmm14);
    asm64::pcmpeqd(xmm6, xmm14);
    asm64::pmaxud(xmm6, xmm9);
    asm64::pcmpeqd(xmm6, xmm9);
    asm64::por(xmm8, xmm6);
    asm64::pcmpeqd({14, 14, 0, 0}, xmm6);
    asm64::movsldup(xmm7, xmm5);
    asm64::pand(xmm5, xmm6);
    asm64::movdqa(xmm14, xmm0);
    asm64::blendvps(xmm0, xmm2, xmm5);
    asm64::movdqa(xmm9, xmm0);
    asm64::blendvps(xmm0, xmm6, xmm2);
    asm64::pand(xmm4, xmm3);
    asm64::xorps(xmm7, xmm7);
    asm64::pcmpeqd(xmm7, xmm3);
    asm64::movaps(xmm5, xmm6);
    asm64::xorps(xmm10, xmm6);
    asm64::movdqa(xmm3, xmm0);
    asm64::blendvps(xmm0, xmm5, xmm6);
    asm64::pand(xmm8, xmm4);
    asm64::movaps(xmm15, xmm3);
    asm64::mulss(xmm15, xmm3);
    asm64::mulss(xmm15, xmm3);
    asm64::pcmpeqd(xmm7, xmm4);
    xmm5 = {6.0f, 0.0f, 0.0f, 0.0f};
    asm64::xorps(xmm2, xmm10);
    asm64::movdqa(xmm4, xmm0);
    asm64::blendvps(xmm0, xmm2, xmm10);
    asm64::movaps(xmm15, xmm0);
    asm64::mulss(xmm5, xmm0);
    xmm2 = {-15.0f, 0.0f, 0.0f, 0.0f};
    asm64::addss(xmm2, xmm0);
    asm64::mulss(xmm15, xmm0);
    xmm4 = {10.0f, 0.0f, 0.0f, 0.0f};
    asm64::addss(xmm4, xmm0);
    asm64::mulss(xmm3, xmm0);
    asm64::minss(xmm11, xmm12);
    asm64::mulss(xmm12, xmm5);
    asm64::addss(xmm2, xmm5);
    asm64::movaps(xmm12, xmm2);
    asm64::mulss(xmm12, xmm2);
    asm64::mulss(xmm12, xmm2);
    asm64::mulss(xmm12, xmm5);
    asm64::addss(xmm4, xmm5);
    asm64::mulss(xmm2, xmm5);
    asm64::addps(xmm13, xmm1);
    asm64::addps(xmm6, xmm10);
    asm64::subps(xmm1, xmm10);
    asm64::movsldup(xmm0, xmm0);
    asm64::mulps(xmm10, xmm0);
    asm64::addps(xmm1, xmm0);
    asm64::movshdup(xmm0, xmm1);
    asm64::subss(xmm1, xmm0);
    asm64::mulss(xmm5, xmm0);
    asm64::addss(xmm1, xmm0);

    return (xmm0.a.f + 0.69f) / 1.483f;
}

float Valheim::clamp01(float value) const
{
    if (value < 0.0f) {
        return 0.0f;
    }
    if (value > 1.0f) {
        return 1.0f;
    }
    return value;
}

float Valheim::lerp(float a, float b, float t) const { return a + (b - a) * clamp01(t); }

float Valheim::length(float x, float y) const { return qSqrt(x * x + y * y); }

float Valheim::lerpStep(float l, float h, float v) const { return clamp01((v - l) / (h - l)); }

float Valheim::smoothStep(float pMin, float pMax, float pX) const
{
    float num = clamp01((pX - pMin) / (pMax - pMin));
    return num * num * (3.0f - 2.0f * num);
}

int32_t Valheim::getStableHashCode(const QString &str)
{
    // stable version of .NET builtin GetHashCode
    uint32_t num1 = 5381;
    uint32_t num2 = num1;
    for (int index = 0; index < str.length() && str[index] != NULL; index += 2) {
        num1 = ((num1 << 5) + num1) ^ str[index].toLatin1();
        if (index != str.length() - 1 && str[index + 1] != NULL)
            num2 = ((num2 << 5) + num2) ^ str[index + 1].toLatin1();
        else
            break;
    }
    return num1 + num2 * 1566083941;
}

void Valheim::setupWorld(int32_t seed, int32_t version)
{
    if (version < 1) {
        m_minMountainDistance = 1500.0f;
    } else {
        m_minMountainDistance = 1000.0f;
    }
    initState(seed);
    m_offset0 = range(-10000, 10000);
    m_offset1 = range(-10000, 10000);
    m_offset2 = range(-10000, 10000);
    m_offset3 = range(-10000, 10000);
    m_riverSeed = range(std::numeric_limits<int32_t>::min(), std::numeric_limits<int32_t>::max());
    m_streamSeed = range(std::numeric_limits<int32_t>::min(), std::numeric_limits<int32_t>::max());
    m_offset4 = range(-10000, 10000);
}

float Valheim::getBaseHeight(float wx, float wy) const
{
    float num2 = length(wx, wy);
    wx += 100000.0f + m_offset0;
    wy += 100000.0f + m_offset1;
    float num3 = 0.0f;
    num3 += perlinNoise(wx * 0.002f * 0.5f, wy * 0.002f * 0.5f) *
            perlinNoise(wx * 0.003f * 0.5f, wy * 0.003f * 0.5f) * 1.0f;
    num3 += perlinNoise(wx * 0.002f * 1.0f, wy * 0.002f * 1.0f) *
            perlinNoise(wx * 0.003f * 1.0f, wy * 0.003f * 1.0f) * num3 * 0.9f;
    num3 += perlinNoise(wx * 0.005f * 1.0f, wy * 0.005f * 1.0f) *
            perlinNoise(wx * 0.01f * 1.0f, wy * 0.01f * 1.0f) * 0.5f * num3;
    num3 -= 0.07f;
    float num4 = perlinNoise(wx * 0.002f * 0.25f + 0.123f, wy * 0.002f * 0.25f + 0.15123f);
    float num5 = perlinNoise(wx * 0.002f * 0.25f + 0.321f, wy * 0.002f * 0.25f + 0.231f);
    float v = qAbs(num4 - num5);
    float num6 = 1.0f - lerpStep(0.02f, 0.12f, v);
    num6 *= smoothStep(744.0f, 1000.0f, num2);
    num3 *= 1.0f - num6;
    if (num2 > 10000.0f) {
        float t = lerpStep(10000.0f, 10500.0f, num2);
        num3 = lerp(num3, -0.2f, t);
        float num7 = 10490.0f;
        if (num2 > num7) {
            float t2 = lerpStep(num7, 10500.0f, num2);
            num3 = lerp(num3, -2.0f, t2);
        }
    }
    if (num2 < m_minMountainDistance && num3 > 0.28f) {
        float t3 = clamp01((num3 - 0.28f) / 0.099999994f);
        num3 = lerp(lerp(0.28f, 0.38f, t3), num3,
                    lerpStep(m_minMountainDistance - 400.0f, m_minMountainDistance, num2));
    }
    return num3;
}

float Valheim::worldAngle(float wx, float wy) const { return qSin(qAtan2(wx, wy) * 20.0f); }

Valheim::Biome Valheim::getBiome(float wx, float wy) const
{
    float magnitude = length(wx, wy);
    float baseHeight = getBaseHeight(wx, wy);
    float num = worldAngle(wx, wy) * 100.0f;
    if (length(wx, wy + -4000.0f) > 12000.0f + num) {
        return Biome::AshLands;
    }
    if (baseHeight <= 0.02f) {
        return Biome::Ocean;
    }
    if (baseHeight <= 0.10f) {
        return Biome::Shallows;
    }
    if (length(wx, wy + 4000.0f) > 12000.0f + num) {
        if (baseHeight > 0.4f) {
            return Biome::Mountain;
        }
        return Biome::DeepNorth;
    }
    if (baseHeight > 0.4f) {
        return Biome::Mountain;
    }
    if (perlinNoise((m_offset0 + wx) * 0.001f, (m_offset0 + wy) * 0.001f) > 0.6f &&
        magnitude > 2000.0f && magnitude < 8000.0f && baseHeight > 0.05f && baseHeight < 0.25f) {
        return Biome::Swamp;
    }
    if (perlinNoise((m_offset4 + wx) * 0.001f, (m_offset4 + wy) * 0.001f) > 0.5f &&
        magnitude > 6000.0f + num && magnitude < 10000.0f) {
        return Biome::Mistlands;
    }
    if (perlinNoise((m_offset1 + wx) * 0.001f, (m_offset1 + wy) * 0.001f) > 0.4f &&
        magnitude > 3000.0f + num && magnitude < 8000.0f) {
        return Biome::Plains;
    }
    if (perlinNoise((m_offset2 + wx) * 0.001f, (m_offset2 + wy) * 0.001f) > 0.4f &&
        magnitude > 600.0f + num && magnitude < 6000.0f) {
        return Biome::BlackForest;
    }
    if (magnitude > 5000.0f + num) {
        return Biome::BlackForest;
    }
    return Biome::Meadows;
}

Valheim::BiomeArea Valheim::getBiomeArea(float wx, float wy) const
{
    auto biome = getBiome(wx, wy);
    auto biome2 = getBiome(wx - -64.0f, wy - -64.0f);
    auto biome3 = getBiome(wx - 64.0f, wy - -64.0f);
    auto biome4 = getBiome(wx - 64.0f, wy - 64.0f);
    auto biome5 = getBiome(wx - -64.0f, wy - 64.0f);
    auto biome6 = getBiome(wx - -64.0f, wy - 0.0f);
    auto biome7 = getBiome(wx - 64.0f, wy - 0.0f);
    auto biome8 = getBiome(wx - 0.0f, wy - -64.0f);
    auto biome9 = getBiome(wx - 0.0f, wy - 64.0f);
    if (biome == biome2 && biome == biome3 && biome == biome4 && biome == biome5 &&
        biome == biome6 && biome == biome7 && biome == biome8 && biome == biome9) {
        return Valheim::Median;
    }
    return Valheim::Edge;
}
