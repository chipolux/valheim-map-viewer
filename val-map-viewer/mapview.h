#ifndef MAPVIEW_H
#define MAPVIEW_H

#include <QImage>
#include <QQuickPaintedItem>

#include "map.h"

class MapView : public QQuickPaintedItem
{
    Q_OBJECT
    QML_ELEMENT

    Q_PROPERTY(Map *map READ map WRITE setMap NOTIFY mapChanged)
    Q_PROPERTY(const QList<MapEntity *> &entities READ entities NOTIFY selectionChanged)
    Q_PROPERTY(const QList<MapStructure *> &structures READ structures NOTIFY selectionChanged)
    Q_PROPERTY(QString selectedBiome READ selectedBiome NOTIFY selectionChanged)
    Q_PROPERTY(float scale READ scale NOTIFY mapScaleChanged)
    Q_PROPERTY(const QPointF offset READ offset NOTIFY offsetChanged)
    Q_PROPERTY(LAYER currentLayer READ currentLayer NOTIFY currentLayerChanged)
    Q_PROPERTY(QString currentLayerName READ currentLayerName NOTIFY currentLayerChanged)

  public:
    enum LAYER {
        LAYER_WORLD,
        LAYER_CRYPTS,
    };
    Q_ENUM(LAYER)

    inline const static QMap<Valheim::Biome, QColor> BiomeColors = {
        {Valheim::Meadows, {0x91, 0xa7, 0x5c}},     {Valheim::AshLands, {0xff, 0x04, 0x00}},
        {Valheim::BlackForest, {0x34, 0x5e, 0x3b}}, {Valheim::Swamp, {0xa3, 0x71, 0x57}},
        {Valheim::DeepNorth, {0xff, 0xff, 0xff}},   {Valheim::Mistlands, {0x52, 0x52, 0x52}},
        {Valheim::Plains, {0xc7, 0xc7, 0x32}},      {Valheim::Mountain, {0xcc, 0xcc, 0xcc}},
        {Valheim::Ocean, {0x00, 0x00, 0xbb}},       {Valheim::Shallows, {0x00, 0x00, 0xcc}},
    };

    explicit MapView(QQuickItem *parent = nullptr);

    void paint(QPainter *painter) override;
    QPointer<Map> map() const { return m_map; }
    void setMap(QPointer<Map> map);
    const QList<MapEntity *> &entities() const { return qAsConst(m_entities); }
    const QList<MapStructure *> &structures() const { return qAsConst(m_structures); }
    float scale() const { return m_scale; }
    const QPointF offset() const { return {m_xOffset, m_zOffset}; }
    LAYER currentLayer() const { return m_currentLayer; }
    QString currentLayerName() const
    {
        switch (m_currentLayer) {
        case LAYER_WORLD:
            return u"World"_qs;
        case LAYER_CRYPTS:
            return u"Crypts"_qs;
        default:
            return u"Unknown"_qs;
        }
    }
    QString selectedBiome()
    {
        return Valheim::BiomeNames[m_map == nullptr || m_map->name().isEmpty()
                                       ? Valheim::Biome::None
                                       : m_map->getBiome(m_selectionRect.center())];
    }

  signals:
    void mapChanged(QPointer<Map> map);
    void selectionChanged();
    void mapScaleChanged(float scale);
    void offsetChanged();
    void currentLayerChanged(MapView::LAYER currentLayer);

  public slots:
    void select(const float mouseX, const float mouseY);
    void zoomIn(const QPointF &around);
    void zoomOut(const QPointF &around);
    void scroll(const QPointF &mouseOffset);
    void fitMap(const bool callUpdate = true);
    void zoomToPoint(const QPointF &point, const float &scale = 10.0f);
    void zoomToSpawn();
    void clearSelection()
    {
        m_entities.clear();
        m_structures.clear();
        m_selectionRect = {0, 0, 0, 0};
        emit selectionChanged();
    }
    void nextLayer()
    {
        switch (m_currentLayer) {
        case LAYER_WORLD:
            m_currentLayer = LAYER_CRYPTS;
            break;
        case LAYER_CRYPTS:
            m_currentLayer = LAYER_WORLD;
            break;
        }
        update();
        emit currentLayerChanged(m_currentLayer);
    }
    void toggleShowAll()
    {
        m_showAll = !m_showAll;
        update();
    }

  private:
    QPointer<Map> m_map;
    QRectF m_visibleRect;
    QRectF m_selectionRect;
    float m_scale;
    float m_xOffset;
    float m_zOffset;
    LAYER m_currentLayer;
    bool m_showAll;

    QImage m_portalIcon;
    QImage m_startTempleIcon;
    QImage m_copperDepositIcon;
    QImage m_tinDepositIcon;
    QImage m_mudPileIcon;
    QImage m_silverDepositIcon;
    QImage m_draugrVillageIcon;
    QImage m_trollCaveIcon;
    QImage m_sunkenCryptIcon;
    QImage m_burialChamberIcon;
    QImage m_fulingVillageIcon;
    QImage m_drakeNestIcon;
    QImage m_leviathanIcon;
    QImage m_haldorIcon;
    QImage m_eikthyrIcon;
    QImage m_theElderIcon;
    QImage m_bonemassIcon;
    QImage m_moderIcon;
    QImage m_yagluthIcon;

    QList<MapEntity *> m_entities;
    QList<MapStructure *> m_structures;
    QList<MapStructure::TYPE> m_cryptTypes;
    QList<MapStructure::TYPE> m_overviewTypes;

    QMap<MapEntity::TYPE, QPicture> m_entityPictures;
    QMap<MapStructure::TYPE, QPicture> m_structurePictures;
    QPicture m_defaultEntityPicture;
    QPicture m_defaultStructurePicture;
    inline QPicture picture(MapEntity::TYPE type) const { return m_entityPictures.value(type); }
    inline QPicture picture(MapStructure::TYPE type) const
    {
        return m_structurePictures.value(type, m_defaultStructurePicture);
    }
    inline const QPair<float, float> getYRange() const
    {
        QPair<float, float> yRange;
        switch (m_currentLayer) {
        case LAYER_WORLD:
            yRange = {-500.0f, 500.0f};
            break;
        case LAYER_CRYPTS:
            yRange = {5000.0f, 5500.0f};
            break;
        }
        return yRange;
    }
    inline bool mapValid() const { return m_map && m_map->loaded(); }

    // three layers are Overview, Surface, and Crypts
    QMap<quint32, QList<QImage>> m_terrainPictures;
    void generateTerrainPictures();
};

#endif // MAPVIEW_H
