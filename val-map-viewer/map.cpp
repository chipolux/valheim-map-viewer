#include "map.h"

#include <QDataStream>
#include <QDebug>
#include <QFile>
#include <algorithm>

#include "dotnet.h"

Map::Map(QObject *parent)
    : QObject(parent)
    , m_valheim(new Valheim(this))
    , m_loaded(false)
    , m_mapPath()
    , m_mapName()
    , m_mapVersion(0)
    , m_terrain()
    , m_entities()
    , m_deposits()
    , m_portals()
    , m_structures()
    , m_maxPosition()
    , m_minPosition()
{
}

const QUrl Map::mapDataFolder() const
{
    QUrl path;

    const QList<QString> homeFolders =
        QStandardPaths::standardLocations(QStandardPaths::HomeLocation);
    const QString homeFolder = homeFolders.first();
    QDir worldsFolder;
#ifdef Q_OS_WIN
    worldsFolder.setPath(homeFolder + "/AppData/LocalLow/IronGate/Valheim/worlds_local");
#else
    worldsFolder.setPath(homeFolder + "/.config/unity3d/IronGate/Valheim/worlds_local");
#endif
    if (worldsFolder.isReadable()) {
        path = QUrl::fromLocalFile(worldsFolder.absolutePath());
        qDebug() << "Found worlds folder:" << path;
    } else {
        path = QUrl::fromLocalFile(homeFolder);
        qDebug() << "Missing worlds folder:" << worldsFolder.absolutePath();
    }

    return path;
}

void Map::loadMap(QUrl filePath)
{
    qDebug() << "Loading map:" << filePath;
    QString errorMessage;
    QUrl dbPath = filePath.toString().replace(".fwl", ".db");
    qDebug() << "  DB file:" << dbPath;

    if (!filePath.isLocalFile()) {
        errorMessage = "Invalid file path: " + filePath.toString();
    }

    QFile file(filePath.toLocalFile());
    QFile db(dbPath.toLocalFile());
    if (!file.open(QIODevice::ReadOnly | QIODevice::ExistingOnly)) {
        errorMessage = "Failed to open file: " + filePath.toLocalFile();
    }
    if (!db.open(QIODevice::ReadOnly | QIODevice::ExistingOnly)) {
        errorMessage = "Failed to open file: " + dbPath.toLocalFile();
    }

    for (MapEntity *entity : qAsConst(m_terrain)) {
        entity->deleteLater();
    }
    m_terrain.clear();
    for (MapEntity *entity : qAsConst(m_entities)) {
        entity->deleteLater();
    }
    m_entities.clear();
    for (MapEntity *entity : qAsConst(m_deposits)) {
        entity->deleteLater();
    }
    m_deposits.clear();
    for (MapEntity *entity : qAsConst(m_portals)) {
        entity->deleteLater();
    }
    m_portals.clear();
    for (MapStructure *structure : qAsConst(m_structures)) {
        structure->deleteLater();
    }
    m_structures.clear();
    m_entitiesByChunk.clear();
    m_maxPosition = {0, 0, 0};
    m_minPosition = {0, 0, 0};
    m_mapPath.clear();
    m_mapName.clear();

    QDataStream stream(&file);
    stream.setByteOrder(QDataStream::LittleEndian);
    stream.setFloatingPointPrecision(QDataStream::SinglePrecision);
    quint32 count = 0;

    // parse world name and seed from FWL
    if (errorMessage.isNull()) {
        stream.skipRawData(4); // skip length, not important
        stream >> m_mapVersion;
        m_mapName = dotnet::readString(stream);
        m_seedName = dotnet::readString(stream);
        stream >> m_seed;
        qDebug() << "  map version:" << m_mapVersion;
        qDebug() << "  map name:" << m_mapName;
        qDebug() << "  seed name:" << m_seedName;
        qDebug() << "  seed:" << m_seed;
    }

    // swap to DB file for reading map data
    m_valheim->setupWorld(m_seed);
    file.close();
    stream.setDevice(&db);

    // parse header, first two bytes are version, rest is unknown
    if (errorMessage.isNull()) {
        stream >> m_mapVersion;
        stream.skipRawData(20);
    }

    // parse entities, see ZDOMan.Load in assembly_valheim.dll
    if (errorMessage.isNull())
        errorMessage = loadEntities(stream);

    // skip dead entities, see ZDOMan in assembly_valheim.dll
    if (errorMessage.isNull()) {
        quint32 deadCount = 0;
        stream >> deadCount;
        qDebug() << "Skipping" << deadCount << "dead entities...";
        stream.skipRawData(20 * deadCount);
    }

    // read chunk list, see ZoneSystem in assembly_valheim.dll
    if (errorMessage.isNull()) {
        quint32 chunkCount = 0;
        stream >> chunkCount;
        qDebug() << "Skipping" << chunkCount << "chunks...";
        // the format is 2 qint32, first for x, second for y, nothing to do
        // with them yet though.
        stream.skipRawData(8 * chunkCount);
        stream.skipRawData(8); // skip 0x6300000016000000, always here
    }

    // parse env variables section, it's just a list of strings like "KilledTroll"
    // etc. that get set when certain events happen in game, seems to be used to
    // allow events like "The ground shakes" troll attack to happen when enough
    // trolls have been killed.
    if (errorMessage.isNull()) {
        quint32 variableCount = 0;
        stream >> variableCount;
        qDebug() << "Skipping" << variableCount << "variables...";
        count = 0;
        while (count < variableCount) {
            quint8 variableLength = 0;
            stream >> variableLength;
            // we just skip the variable strings for now
            stream.skipRawData(variableLength);
            count++;
        }
        // skip a byte that is always 0x01, not sure what it is part of or means
        stream.skipRawData(1);
    }

    // parse structures, these are simple entries with a string identifier
    // and location that indicate where they should be placed, things like the
    // StartTemple, Eikthyr altars, SunkenCrypts, StoneCircles, etc.
    if (errorMessage.isNull())
        errorMessage = loadStructures(stream);

    // skip random event data, see RandEventSystem in assembly_valheim.dll
    if (errorMessage.isNull()) {
        int skippedBytes = stream.skipRawData(21);
        if (skippedBytes != 21) {
            errorMessage = "Premature end of world file.";
        }
    }

    db.close();
    m_loaded = false;
    if (errorMessage.isNull()) {
        qDebug() << "Map file loaded!";
        m_loaded = true;
        m_mapPath = filePath;
        postLoad();
    } else {
        qWarning() << "Failed to load map file:" << errorMessage;
    }
    emit mapLoaded(errorMessage);
}

QString Map::loadEntities(QDataStream &stream)
{
    QString errorMessage;
    quint32 entityCount = 0;
    stream >> entityCount;
    qDebug() << "Loading" << entityCount << "entities...";
    quint32 count = 0;
    while (count < entityCount) {
        if (stream.atEnd()) {
            errorMessage = u"EOF before reading entity %1!"_qs.arg(count);
            break;
        }
        qint64 offset = stream.device()->pos();
        qint64 userId;
        quint32 id;
        qint32 bodyLength;
        stream >> userId;
        stream >> id;
        stream >> bodyLength;
        if (bodyLength > 0) {
            QByteArray data(bodyLength, '\0');
            stream.readRawData(data.data(), bodyLength);
            MapEntity *entity = new MapEntity(data, offset, userId, id, m_mapVersion, this);
            const float x = entity->position().x();
            const float y = entity->position().y();
            const float z = entity->position().z();
            if (x <= 20000 && z <= 20000) {
                m_maxPosition.setX(qMax(x, m_maxPosition.x()));
                m_maxPosition.setY(qMax(y, m_maxPosition.y()));
                m_maxPosition.setZ(qMax(z, m_maxPosition.z()));
                m_minPosition.setX(qMin(x, m_minPosition.x()));
                m_minPosition.setY(qMin(y, m_minPosition.y()));
                m_minPosition.setZ(qMin(z, m_minPosition.z()));
            }
            switch (entity->type()) {
            case MapEntity::TERRAIN:
                entity->setNWBiome(m_valheim->getBiome(x - 32.0f, z + 32.0f));
                entity->setNEBiome(m_valheim->getBiome(x + 32.0f, z + 32.0f));
                entity->setSWBiome(m_valheim->getBiome(x - 32.0f, z - 32.0f));
                entity->setSEBiome(m_valheim->getBiome(x + 32.0f, z - 32.0f));
                m_terrain.append(entity);
                break;
            case MapEntity::PORTAL:
                m_portals.append(entity);
                break;
            case MapEntity::LEVIATHAN:
            case MapEntity::COPPER_DEPOSIT:
            case MapEntity::TIN_DEPOSIT:
            case MapEntity::SILVER_DEPOSIT:
            case MapEntity::MUDDY_SCRAP_PILE_1:
            case MapEntity::MUDDY_SCRAP_PILE_2:
                m_deposits.append(entity);
                break;
            default:
                m_entities.append(entity);
                m_entitiesByChunk[entity->chunk()].prepend(entity);
                break;
            }
        }
        count++;
    }
    qDebug() << "  " << m_terrain.length() << "terrain blocks...";
    qDebug() << "  " << m_entities.length() << "entities...";
    qDebug() << "  " << m_deposits.length() << "deposits...";
    qDebug() << "  " << m_portals.length() << "portals...";
    return errorMessage;
}

QString Map::loadStructures(QDataStream &stream)
{
    QString errorMessage;
    quint32 structureCount = 0;
    stream >> structureCount;
    qDebug() << "Loading" << structureCount << "structures...";
    quint32 count = 0;
    while (count < structureCount) {
        if (stream.atEnd()) {
            errorMessage = "EOF while reading structures!";
            break;
        }
        m_structures.append(new MapStructure(stream, this));
        count++;
    }
    return errorMessage;
}

void Map::postLoad() const {}

const QString Map::getRawEntity(const quint32 &offset) const
{
    QString data;
    QFile f(m_mapPath.toLocalFile());
    if (f.open(QFile::ReadOnly)) {
        QDataStream in(&f);
        in.setByteOrder(QDataStream::LittleEndian);
        in.setFloatingPointPrecision(QDataStream::SinglePrecision);
        in.skipRawData(offset);
        char head[12]; // marker and id
        quint32 length;
        in.readRawData(head, 12);
        in >> length;
        char *body = new char[length];
        in.readRawData(body, length);
        f.close();

        QByteArray outBuf;
        QDataStream out(&outBuf, QIODevice::WriteOnly);
        out.setByteOrder(QDataStream::LittleEndian);
        out.setFloatingPointPrecision(QDataStream::SinglePrecision);
        out.writeRawData(head, 12);
        out << length;
        out.writeRawData(body, length);
        delete[] body;
        data = outBuf.toHex();
    } else {
        qWarning() << "Failed to open:" << m_mapPath;
    }
    return data;
}
