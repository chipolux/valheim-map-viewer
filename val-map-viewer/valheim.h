/* XORSHIFT 128 Psuedorandom Implementation
 * Trying to duplicate Unity's UnityEngine::Random
 * See: https://en.wikipedia.org/wiki/Xorshift
 *      https://docs.unity3d.com/ScriptReference/Random.html
 */
#ifndef VALHEIM_H
#define VALHEIM_H

#include <QMap>
#include <QObject>

class Valheim : public QObject
{
    Q_OBJECT

  public:
    explicit Valheim(QObject *parent = nullptr);

    // Unity Random
    struct State {
        int32_t a, // 0x0 | s0 in Unity
            b,     // 0x8 | s1 in Unity
            c,     // 0x4 | s2 in Unity
            d;     // 0xc | s3 in Unity
        operator QString() const { return QString("%1, %2, %3, %4").arg(a).arg(b).arg(c).arg(d); }
    };
    void initState(int32_t seed);
    float range(float minInclusive, float maxInclusive);
    int32_t range(int32_t minInclusive, int32_t maxInclusive);
    float getValue();
    State getState() const { return m_state; }
    void setState(State state) { m_state = state; }

    // Unity Mathf
    float perlinNoise(float x, float y) const;
    float clamp01(float value) const;
    float lerp(float a, float b, float t) const;

    // Valheim Utils
    float length(float x, float y) const;
    float lerpStep(float l, float h, float v) const;
    float smoothStep(float pMin, float pMax, float pX) const;
    int32_t getStableHashCode(const QString &str);

    // Valheim WorldGenerator
    enum Biome {
        None,
        Meadows,
        Swamp,
        Mountain,
        BlackForest,
        Plains,
        AshLands,
        DeepNorth,
        Ocean,
        Mistlands,
        Shallows,
    };
    Q_ENUM(Biome)
    enum BiomeArea {
        Median,
        Edge,
    };
    Q_ENUM(BiomeArea)
    void setupWorld(int32_t seed, int32_t version = 1);
    float getBaseHeight(float wx, float wy) const;
    float worldAngle(float wx, float wy) const;
    Biome getBiome(float wx, float wy) const;
    BiomeArea getBiomeArea(float wx, float wy) const;

    inline const static QMap<Biome, QString> BiomeNames = {
        {None, "None"},           {Meadows, "Meadows"},          {Swamp, "Swamp"},
        {Mountain, "Mountain"},   {BlackForest, "Black Forest"}, {Plains, "Plains"},
        {AshLands, "Ashlands"},   {DeepNorth, "Deep North"},     {Ocean, "Ocean"},
        {Mistlands, "Mistlands"}, {Shallows, "Shallows"},
    };

  private:
    State m_state;
    void incrementState();
    inline const static int32_t permutations[512] = {
        151, 160, 137, 91,  90,  15,  131, 13,  201, 95,  96,  53,  194, 233, 7,   225, 140, 36,
        103, 30,  69,  142, 8,   99,  37,  240, 21,  10,  23,  190, 6,   148, 247, 120, 234, 75,
        0,   26,  197, 62,  94,  252, 219, 203, 117, 35,  11,  32,  57,  177, 33,  88,  237, 149,
        56,  87,  174, 20,  125, 136, 171, 168, 68,  175, 74,  165, 71,  134, 139, 48,  27,  166,
        77,  146, 158, 231, 83,  111, 229, 122, 60,  211, 133, 230, 220, 105, 92,  41,  55,  46,
        245, 40,  244, 102, 143, 54,  65,  25,  63,  161, 1,   216, 80,  73,  209, 76,  132, 187,
        208, 89,  18,  169, 200, 196, 135, 130, 116, 188, 159, 86,  164, 100, 109, 198, 173, 186,
        3,   64,  52,  217, 226, 250, 124, 123, 5,   202, 38,  147, 118, 126, 255, 82,  85,  212,
        207, 206, 59,  227, 47,  16,  58,  17,  182, 189, 28,  42,  223, 183, 170, 213, 119, 248,
        152, 2,   44,  154, 163, 70,  221, 153, 101, 155, 167, 43,  172, 9,   129, 22,  39,  253,
        19,  98,  108, 110, 79,  113, 224, 232, 178, 185, 112, 104, 218, 246, 97,  228, 251, 34,
        242, 193, 238, 210, 144, 12,  191, 179, 162, 241, 81,  51,  145, 235, 249, 14,  239, 107,
        49,  192, 214, 31,  181, 199, 106, 157, 184, 84,  204, 176, 115, 121, 50,  45,  127, 4,
        150, 254, 138, 236, 205, 93,  222, 114, 67,  29,  24,  72,  243, 141, 128, 195, 78,  66,
        215, 61,  156, 180, 151, 160, 137, 91,  90,  15,  131, 13,  201, 95,  96,  53,  194, 233,
        7,   225, 140, 36,  103, 30,  69,  142, 8,   99,  37,  240, 21,  10,  23,  190, 6,   148,
        247, 120, 234, 75,  0,   26,  197, 62,  94,  252, 219, 203, 117, 35,  11,  32,  57,  177,
        33,  88,  237, 149, 56,  87,  174, 20,  125, 136, 171, 168, 68,  175, 74,  165, 71,  134,
        139, 48,  27,  166, 77,  146, 158, 231, 83,  111, 229, 122, 60,  211, 133, 230, 220, 105,
        92,  41,  55,  46,  245, 40,  244, 102, 143, 54,  65,  25,  63,  161, 1,   216, 80,  73,
        209, 76,  132, 187, 208, 89,  18,  169, 200, 196, 135, 130, 116, 188, 159, 86,  164, 100,
        109, 198, 173, 186, 3,   64,  52,  217, 226, 250, 124, 123, 5,   202, 38,  147, 118, 126,
        255, 82,  85,  212, 207, 206, 59,  227, 47,  16,  58,  17,  182, 189, 28,  42,  223, 183,
        170, 213, 119, 248, 152, 2,   44,  154, 163, 70,  221, 153, 101, 155, 167, 43,  172, 9,
        129, 22,  39,  253, 19,  98,  108, 110, 79,  113, 224, 232, 178, 185, 112, 104, 218, 246,
        97,  228, 251, 34,  242, 193, 238, 210, 144, 12,  191, 179, 162, 241, 81,  51,  145, 235,
        249, 14,  239, 107, 49,  192, 214, 31,  181, 199, 106, 157, 184, 84,  204, 176, 115, 121,
        50,  45,  127, 4,   150, 254, 138, 236, 205, 93,  222, 114, 67,  29,  24,  72,  243, 141,
        128, 195, 78,  66,  215, 61,  156, 180};
    float m_offset0;
    float m_offset1;
    float m_offset2;
    float m_offset3;
    float m_offset4;
    int32_t m_riverSeed;
    int32_t m_streamSeed;
    float m_minMountainDistance;
};

#endif // VALHEIM_H
