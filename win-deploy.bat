:: start from Qt build command prompt

:: cleanup build dir and prep for new build
rmdir build-win-deploy /s /q
mkdir build-win-deploy
cd build-win-deploy

:: build application
qmake ..\val-map-viewer\val-map-viewer.pro
mingw32-make -j4

:: prep for windeployqt
mingw32-make clean

:: bring in dependencies with windeployqt and zip it all up
windeployqt --compiler-runtime --qmldir ..\val-map-viewer ".\release\Valheim Map Viewer.exe"
..\tools\7za.exe a -tzip "Valheim Map Viewer.zip" .\release\*

cd ..
echo Done!
