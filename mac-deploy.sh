#!/bin/bash
set -e
. .env

# cleanup and setup build directory
rm -rf build-macos-deploy
mkdir build-macos-deploy
cd build-macos-deploy

# build application
$QT_BIN/qmake ../val-map-viewer/val-map-viewer.pro
make -j8

# prep for macdeployqt
make clean
rm Makefile

# build distributable dmg
echo "Bundling for distribution..."
$QT_BIN/macdeployqt "Valheim Map Viewer.app" \
    -qmldir=../val-map-viewer \
    -sign-for-notarization=$CODESIGN_IDENT

echo "Done!"
