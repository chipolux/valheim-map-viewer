#!/bin/bash
set -e
. .env

LINUX_BUILD=build-linux-deploy/Valheim_Map_Viewer-x86_64.AppImage
MACOS_BUILD=build-macos-deploy/Valheim\ Map\ Viewer.app
WIN_BUILD=build-win-deploy/Valheim\ Map\ Viewer.zip

if [ -f "$LINUX_BUILD" ]; then
    "$BUTLER" push --if-changed --fix-permissions "$LINUX_BUILD" chipolux/valheim-map-viewer:linux-64bit
    rm -f "$LINUX_BUILD"
    echo "Pushed linux build!"
fi

if [ -d "$MACOS_BUILD" ]; then
    "$BUTLER" push --if-changed --fix-permissions --auto-wrap "$MACOS_BUILD" chipolux/valheim-map-viewer:mac
    rm -rf "$MACOS_BUILD"
    echo "Pushed macos build!"
fi

if [ -f "$WIN_BUILD" ]; then
    "$BUTLER" push "$WIN_BUILD" chipolux/valheim-map-viewer:win-64bit
    rm -f "$WIN_BUILD"
    echo "Pushed windows build!"
fi

echo "Done!"
