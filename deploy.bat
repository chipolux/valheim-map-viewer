@echo off
:: Requires butler to be installed via itch in the default location
set BUTLER=%APPDATA%\itch\apps\butler\butler.exe
set LINUX_BUILD=build-linux-deploy\Valheim_Map_Viewer-x86_64.AppImage
set MACOS_BUILD=build-macos-deploy\Valheim Map Viewer.app
set WIN_BUILD=build-win-deploy\Valheim Map Viewer.zip

if exist "%LINUX_BUILD%" (
    "%BUTLER%" push --if-changed --fix-permissions "%LINUX_BUILD%" chipolux/valheim-map-viewer:linux-64bit
    del /f /q "%LINUX_BUILD%"
    echo Pushed linux build!
)

if exist "%MACOS_BUILD%" (
    "%BUTLER%" push --if-changed --fix-permissions --auto-wrap "%MACOS_BUILD%" chipolux/valheim-map-viewer:mac
    del /f /q "%MACOS_BUILD%"
    echo Pushed macos build!
)

if exist "%WIN_BUILD%" (
    "%BUTLER%" push --if-changed "%WIN_BUILD%" chipolux/valheim-map-viewer:win-64bit
    del /f /q "%WIN_BUILD%"
    echo Pushed windows build!
)

echo Done!
