#!/bin/bash
set -e
. .env

# cleanup and setup build directory
rm -rf build-linux-deploy
mkdir build-linux-deploy
cd build-linux-deploy

# build application
$QT_BIN/qmake ../val-map-viewer/val-map-viewer.pro
make -j8

# prep for macdeployqt
make clean
rm Makefile

# build distributable
echo "Bundling for distribution..."
mkdir -p usr/bin usr/lib usr/share/applications usr/share/icons/hicolor/32x32/apps
mv Valheim\ Map\ Viewer usr/bin/ValheimMapViewer
cp ../val-map-viewer/val-map-viewer.desktop usr/share/applications/Valheim\ Map\ Viewer.desktop
cp ../val-map-viewer/resources/icon.png usr/share/icons/hicolor/32x32/apps/ValheimMapViewer.png
$LINUXDEPLOYQT usr/share/applications/Valheim\ Map\ Viewer.desktop \
    -appimage \
    -qmake=$QT_BIN/qmake \
    -qmldir=../val-map-viewer
mv Valheim_Map_Viewer-*-x86_64.AppImage Valheim_Map_Viewer-x86_64.AppImage

echo "Done!"
